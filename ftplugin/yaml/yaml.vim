if exists( 'g:vimine_ftplugin_yaml' )
    finish
endif
let g:vimine_ftplugin_yaml = 1

command! DisableCopInRubocop lua require'ft.yaml'.disable_cop_in_rubocop()
"SPDX-License-Identifier: Apache-2.0
