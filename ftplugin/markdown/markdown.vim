if exists( "b:did_vimine_markdown" )
  finish
endif
let b:did_vimine_markdown = 1

function! s:makeIssueHelp()
  echo "usage:"
  echo "MakeIssue params"
  echo ""
  echo " where params"
  echo ""
  echo "user:<user> defaults to 'robertdober'"
  echo "repo:<repo> defaults to 'earmark_parser'"
  echo ""
  echo "Needs to be executed at a line of the following format"
  echo "   'issue-nb description'"
endfunction

function! s:makeIssue(...)
  " key:value params parsing
  let [l:params, _] = luaeval('require"tools.args".parse_args("' . join(a:000) . '")')
  let l:user = get(l:params, 'user', 'robertdober')
  let l:repo = get(l:params, 'repo', 'earmark_parser')
  let l:type = get(l:params, 'type', 'issues') " or pulls

  let l:url = "https://github.com/" . l:user . "/" . l:repo . "/" . l:type . "/"

  " Current line parsing
  let l:line = getline('.')
  let l:prefix = substitute(l:line, '\S.*', '', '')
  let l:description = substitute(l:line, '^\s*', '', '')
  let l:parts = split(l:description)
  let l:number = l:parts[0]

  " Combine params and current line
  let l:url = '(https://github.com/' . l:user . '/' . l:repo . '/' . l:type . '/' . l:number . ')'
  let l:text = '- [' . join(l:parts[1:-1]) . ']'

  " Change current line
  call setline('.', l:prefix . l:text . l:url)
endfunction

function! s:setRepoName(name)
  let b:repo_name = a:name
endfunction

command! -nargs=* MakeIssue call <SID>makeIssue(<q-args>)
command! MakeIssueHelp call <SID>makeIssueHelp()
command! -nargs=1 SetRepoName call<SID>setRepoName(<q-args>)
command! SetRepoToEarmark call <SID>setRepoName('pragdave/earmark')
command! SetRepoToEarmarkParser call <SID>setRepoName('robertdober/earmark_parser')

" better to use ccompletion
abbrev icode `
abbrev code ```
abbrev ecode ```elixir
abbrev rcode ```ruby
