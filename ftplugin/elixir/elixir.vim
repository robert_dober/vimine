if exists( "b:did_vimine_elixir" )
  finish
endif
let b:did_vimine_elixir = 1

let s:save_cpo = &cpo " save user coptions
set cpo&vim " reset them to defaults

if !exists('g:lab42_tmux_elixir_test_window')
  let g:lab42_tmux_elixir_test_window = 'tests'
endif

function! s:insert_debug_line() " {{{{{
  let l:line = getline(".") 
  let l:prefix = substitute(l:line, '\v\S.*', '', '')
  call append(line("."), l:prefix . '|> IO.inspect')
endfunction " }}}}}
" command! IOInspect :call lab42#elixir#insert_inspect()
command! ElixirActivateInsertDebugInspect call buffer#uncomment_suffix(' |> IO.inspect')
command! ElixirDeactivateInsertDebugInspect call buffer#comment_suffix(' |> IO.inspect')
command! ElixirInsertDebugInspect call buffer#insert_at_cursor(' |> IO.inspect() ')
command! ElixirInsertDebugLine call <SID>insert_debug_line()
nmap <Leader>i :ElixirInsertDebugInspect<CR>

function! s:formatThisFile() " {{{{{
  write
  call system('mix format ' . expand('%'))
  edit!
endfunction " }}}}}
command! FormatThisFile call <SID>formatThisFile()
nmap <Leader>f :FormatThisFile<CR>


command! -range=% RenumberIex lua require'filter'.filter_with(<line1>, <line2>, require'filters.iex_renumber')

function! s:getLastLine(lnb) " {{{{{
  let l:lnb = a:lnb + 1
  let l:last_lnb = line('$')
  while l:lnb <= l:last_lnb
    let l:line = getline(l:lnb)
    if l:line[0] == " " 
      return l:lnb - 1
    endif
    let l:lnb += 1
  endwhile
  return l:last_lnb
endfunction " }}}}}

function! s:parseMarkdown(line1, line2) " {{{{
  let l:prefix = substitute(getline(a:line1), '\S.*', '', '')
  let l:remove = len(l:prefix)
  let l:lines  = getline(a:line1, a:line2)
  let l:lines  = map(l:lines, 'v:val[' . l:remove . ':]')
  call append(a:line2 + 2, l:lines)
  let l:line3  = a:line2 + 3
  let l:line4  = l:line3 + a:line2 - a:line1
  exec l:line3 . ',' . l:line4 . '!cmark-gfm'
  let l:line5  = s:getLastLine(l:line3)
  exec l:line3 . ',' . l:line5 . '!' . g:vimine_home . '/ext/elixir/parse_html/parse_html'
endfunction " }}}}}
command! -range ParseMarkdown call <SID>parseMarkdown(<line1>, <line2>)

map <Leader>d Odoc<C-c>
map <Leader>m Omoduledoc<C-c>
map <Leader>s Ospec<C-c>
map <Leader>x iiex><C-c>
imap <Leader>x iex><C-c>

map <Leader>z a<Space>><C-c>
imap <Leader>z  <Space>><C-c>
imap <Leader>g >
" imap <Leader><Leader> >
map <Leader>tti :call tmux#test('iex -S ')<CR>


let b:ale_fixers = { 'elixir': ['mix_format'] }
" imap @@@ |>
" imap @@ >

let g:gfm_file = '/tmp/xxx-gfm.md'
let s:html_file = '/tmp/xxx-gfm.html'
function! s:write_to_gfm_file(...)
  lua << EOL
local writefile = require'myio'.writefile
local align     = require'tools.lines'.align_to_first_line
local api       = require'nvimapi'

local _, _, lines = api.get_selected_lines()
local aligned = align(lines)
local filename = vim.api.nvim_get_var("gfm_file")
writefile(filename, aligned)
EOL
  let l:commands = [
        \ 'cmark-gfm ' . g:gfm_file . ' > ' . s:html_file,
        \ 'bat -p ' . g:gfm_file,
        \ 'echo "-----------------------------"',
        \ 'bat -p ' . s:html_file ]
  call tmux#send_and_switch('-1', join(l:commands, "; "))
endfunction
command! -range WriteToGfmFile call s:write_to_gfm_file(<line1>, <line2>)
vnoremap <Leader>gfm :WriteToGfmFile<CR>
noremap <Leader>gfm :WriteToGfmFile<CR>

let g:usage_macro = "ExUnit.Case"
let &cpo = s:save_cpo " and restore after
unlet s:save_cpo
