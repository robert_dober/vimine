if exists( 'g:vimine_ftplugin_ruby' )
  if !g:vimine_force_reload
    finish
  endif
endif
let g:vimine_ftplugin_ruby = 1

function! s:set_local_tmux_test_command(cmd)
  let l:cmd = a:cmd . ' '
  let b:local_tmux_test_command = l:cmd

endfunction

command! -nargs=1 SetLocalTmuxTestCommand call <SID>set_local_tmux_test_command(<q-args>)

function! s:debugger(text) " {{{{{
  let l:prefix = substitute(getline("."), '\v\S.*', '', '')
  call append('.', l:prefix . a:text)
  exec 'w!'
endfunction " }}}}}

command! Debugger call <SID>debugger('debugger')

function! s:shell_command(command)
  exec 'w!'
  exec '!' . a:command
  exec 'e!'
endfunction
"-------------------------------------------------------------
" Rubocop
"-------------------------------------------------------------
command! RubocopA call <SID>shell_command('rubocop -A %')
command! Rubocopa call <SID>shell_command('rubocop -a %')
command! ToggleMethodType lua require'ft.ruby'.toggle_method_type()

"-------------------------------------------------------------
" Module Structure
"-------------------------------------------------------------

command! ModuleStructure lua require'ft.ruby'.make_module_structure()

"-------------------------------------------------------------
" Sort Parts
"-------------------------------------------------------------

command! -range=% SortMethods :lua require"selection.sort".sort_by_header('^%s*def%s(.*)', <line1>, <line2>)

"-------------------------------------------------------------
" New File
"-------------------------------------------------------------

function! s:new_file()
  let @a=":CCComplete:AddApacheSPXLicenceIdentifiero:ModuleStructureA"
  echo "call macro a for initialisation of your file"
endfunction

command! NewFile call <SID>new_file()

function! s:ruby_template()
  lua << LUA
  require 'cccomplete'.complete(1,1)
  require 'ft.ruby'.make_module_structure()
  vim.api.nvim_command("AddApacheSPXLicenceIdentifier")
LUA
endfunction

command! RubyTemplate call <SID>ruby_template()
" SPDX-License-Identifier: Apache-2.0
