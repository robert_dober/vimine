" if exists( "b:did_vimine_haskell" )
"   finish
" endif
let b:did_vimine_haskell = 1

let s:save_cpo = &cpo " save user coptions
set cpo&vim " reset them to defaults

set sw=4 sts=4

let &cpo = s:save_cpo " and restore after
unlet s:save_cpo
