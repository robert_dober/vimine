let s:openTags = { 
      \ 'body': 1,
      \ 'div': 1,
      \ 'head': 1,
  \ 'nav': 1,
  \ 'section': 1,
  \ 'table': 1 }

let s:stack = []

function! s:maybeUpOne(line)
  let l:matches = matchlist(a:line, '\v\<\/([a-zA-Z]+)\>\s*$')
  " No closing tag, keep level {{{2
  if empty(l:matches)
    return "="
  endif
  let l:tag = l:matches[1]
  if tolower(l:tag) == tolower(get(s:stack, 0) || '')
    let s:stack = s:stack[1:]
    return "s1"
  endif
  return "="
endfunction

function! folders#debugStack()
  return s:stack
endfunction

function! folders#htmlFold(lnum)
  " Reinitialize stack, edited document may not be complete {{{2
  if v:lnum == 1
    let s:stack = []
  endif
  let l:line = getline(a:lnum)
  let l:matches = matchlist(l:line, '\v^\s*\<([a-zA-Z]+)')
  " No opening tag, carry on {{{2
  if empty(l:matches)
    return s:maybeUpOne(l:line)
  endif
  let l:tag = l:matches[1]
  " Not a compound tag, carry on {{{2
  if !get(s:openTags, tolower(l:tag))
    return "="
  endif
  let l:end_match = match(l:line, '\v\<\/' . l:tag . '\>\s*$')
  " tag ends same line, keep level {{{2
  if l:end_match > 0
    return "="
  endif
  " tag autocloses, keep level {{{2
  let l:end_match = match(l:line, '\v\/\>.*$')
  if l:end_match > 0
    call insert(s:stack, l:tag)
    return "="
  endif
  " finally we can add one level {{{2
  return "a1"
endfunction
