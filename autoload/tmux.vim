
" Local {{{
" =====
function! s:haskellHspecTestCommand()
  let g:__haskell_test_command__=""
  lua require'testing/haskell_hspec'.run("__haskell_test_command__")
  return g:__haskell_test_command__
endfunction

let s:test_commands = {
  \ 'crystal': 'crystal spec ',
  \ 'elixir': 'mix test ',
  \ 'haskell': funcref("s:haskellHspecTestCommand"),
  \ 'javascript': 'mocha ',
  \ 'lua': 'busted -c ',
  \ 'python': 'pytest ',
  \ 'ruby': 'bundle exec rspec ',
  \ }

let s:specific_line_triggers = {
  \ 'crystal': '\v^\s*(describe\s|context\s|it\s)',
  \ 'elixir': '\v^\s*(describe\s|context\s|test\s)',
  \ 'haskell': '\v^\s*(describe\s|it\s)',
  \ 'ruby': '\v^\s*(describe\s|context\s|it\s)'
  \ }

let s:funcref_type = 2
let s:string_type = type("")

function! s:compile_window(window) " {{{{{
  let l:specifier = ':='
  if match(a:window, '\v[-+]') == 0
    let l:specifier = ':'
  endif
  return ' -t "' . l:specifier . a:window . '" '
endfunction: " }}}}}

function! s:get_file_and_lnb() " {{{{{
  let l:file = expand('%')
  let l:line = getline('.')
  let l:trigger = get(s:specific_line_triggers, &ft, 'none')
  " call dbg#ts()
  " call dbg#dbg(l:trigger)
  if l:trigger == 'none'
    return l:file
  else
    " call dbg#dbg('in trigger branch')
    let l:match = match(l:line, l:trigger)
    " call dbg#dbg( 'match: ' . l:match . ', line: ' .l:line)
    if l:match == 0
      " call dbg#dbg("matched")
      return l:file . ':' . line('.')
    else
      return l:file
    endif
  end
endfunction " }}}}}

function! s:goto(window) " {{{{{
  silent exec "write"
  let l:window = s:compile_window(a:window)
  call s:switch_to(l:window)
endfunction " }}}}}

function! s:send_and_switch(window, keys) " {{{{{
  for l:key in a:keys
    let l:Command = 'tmux send-keys -l' . a:window . shellescape(l:key)
     echomsg l:Command
    call dbg#ts()
    call dbg#dbg(a:window)

    call system(l:Command)
    call system('tmux send-keys' . a:window . 'C-m')
  endfor
  call s:switch_to(a:window)
endfunction " }}}}}

function! s:switch_to(window) " {{{{{
  call system('tmux select-window' . a:window)
endfunction " }}}}}
" }}}

" API {{{
" ===
function! tmux#again(window) " {{{{{
  silent exec "write"
  let l:window = s:compile_window(a:window)
  let l:Command = 'tmux send-keys' . l:window . 'Up C-m'
  call system(l:Command)
  call s:switch_to(l:window)
endfunction " }}}}}

function! tmux#goto() " {{{{{
  if exists('g:vimine_tmux_goto_window')
    call s:goto(g:vimine_tmux_goto_window)
  else
    echoerr "Define g:vimine_tmux_goto_window to use this function"
  endif
endfunction " }}}}}

" tmux#send_and_switch(ndow,
function! tmux#send_and_switch(window, keys) " {{{{{
  let l:window = s:compile_window(a:window)
  if type(a:keys) == s:string_type
    call s:send_and_switch(l:window, [a:keys])
  else
    call s:send_and_switch(l:window, a:keys)
  endif
endfunction " }}}}}

function! tmux#test(...) " {{{{{
  let l:test_window='tests'
  if exists('b:tmux_test_window')
    let l:test_window = b:tmux_test_window
  endif
  let l:prefix = ''
  if exists('b:local_tmux_test_command')
    let l:Command = b:local_tmux_test_command
  else
    let l:Command = get(s:test_commands, &ft)
  endif
  if type(l:Command) == s:funcref_type
    let l:complete_command = l:Command()
  else
    if a:0 && a:1 == 'all'
      let l:file = ''
    elseif a:0
      let l:prefix = a:1
      let l:file = s:get_file_and_lnb()
      echom 'Prefix set to ' . l:prefix
    else
      let l:file = s:get_file_and_lnb()
    endif
    let l:complete_command = l:prefix . l:Command . l:file
    " echo "complete: " . l:complete_command
  endif
  silent exec "write"
  call tmux#send_and_switch(l:test_window, l:complete_command)
endfunction: " }}}}}
" }}}
