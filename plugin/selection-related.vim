if exists( 'g:vimine_selection_related' )
  finish
endif
let g:vimine_selection_related = 1

let s:save_cpo = &cpo " save user coptions
set cpo&vim " reset them to defaults

command! -range -nargs=1 SortSelection lua require'selection_command'.execute('sort', <args>)
command! -range SortCommaSepList lua require'selection_command'.execute('sort', ', ')
command! -range SortSemicolonSepList lua require'selection_command'.execute('sort', ', ')

let &cpo = s:save_cpo " and restore after
unlet s:save_cpo

