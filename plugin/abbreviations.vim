if exists( 'g:vimine_did_abbreviations' )
  if !g:vimine_force_reload
    finish
  endif
endif
let g:vimine_did_abbreviations = 1

abbrev OTOH on the other hand
