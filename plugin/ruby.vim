function! s:ruby_filter(name, l1, l2,...)
  let l:prefix = a:l1 . "," . a:l2 ."!ASDF_RUBY_VERSION=3.1.2 ruby "
  let l:location = g:vimine_home . "/filters/ruby/" . a:name . ".rb "

  let l:args = []
  if a:0
    let l:args = split(a:1)
  endif

  let l:command = l:prefix . l:location . join(l:args)
  " echo '''' . l:command . ''''
  exec l:command
endfunction

command! -nargs=* -range=% RbCarthesian call <SID>ruby_filter('carthesian', <line1>, <line2>, <q-args>)
command! -nargs=* -range=% RbExpandList call <SID>ruby_filter('expand_list', <line1>, <line2>, <q-args>)
command! -nargs=* -range=% RbIntercale call <SID>ruby_filter('intercale', <line1>, <line2>, <q-args>)
command! -nargs=* -range=% RbToCsv call <SID>ruby_filter('to_csv', <line1>, <line2>, <q-args>)
command! -nargs=* -range=% RbToSheet call <SID>ruby_filter('to_sheet', <line1>, <line2>, <q-args>)

command! -range RbExpandAttachment call <SID>ruby_filter('expand_attachment', <line1>, <line2>)

