if !exists("g:vimine_home")
  let g:vimine_home = expand("<sfile>:p:h:h")
endif

if exists("g:vimine_loaded")
  if !g:vimine_force_reload
    finish
  endif
endif

let g:vimine_loaded = 1

let g:vimine_version = "1.0.3"
command! VimineVersion :echo g:vimine_version

" if has('ruby')
"   exec 'ruby $Vimine="' . g:vimine_home . '"'
"   exec 'ruby $:.unshift("' . g:vimine_home . '/ext/ruby/lib")'
" endif
function! s:cleanup() " {{{{{
  let l:lnb = line('.')
  silent exec '%s/\s\+$//'
  silent exec 'write'
  exec 'normal ' . l:lnb . 'G'
endfunction " }}}}}

function! s:dhallSettings()
  set ft=haskell
  set sw=2 sts=2 expandtab tw=0 nu
endfunction
au BufNewFile,BufRead *.{dhall} call s:dhallSettings()

command! CheckoutBuffer :call system("git checkout " . expand("%:p") ) | edit!
command! Cleanup call <SID>cleanup()
command! MakeRuler call buffer#insert_ruler()
command! Debug call pry#insert_debug()
command! Pry call pry#insert_pry()
command! UnPry call pry#remove_all_pries()
command! WriteAndSource :w|source %
command! SqueezeSpaces :s/\v>\s+</ /g
" Bad mapping
" inoremap <C-p> <Esc>:SqueezeSpaces<CR>a

map <Leader>tnt :tabnew<CR><C-t>
function! s:tnerd(dir)
  silent exe "tabnew"
  silent exe "NERDTree " . a:dir
endfunction
command! -complete=dir -nargs=1 TNERD :call <SID>tnerd(<q-args>)

nnoremap gF :call tab#open_file_under_cursor()<CR>
nnoremap <C-t> :FuzzyOpen<CR>
vnoremap <Leader>wt :w! /tmp/xxx<CR>
nnoremap <Leader>rt :read /tmp/xxx<CR>
for i in range(10)
  exec 'vnoremap <Leader>' . i . 'wt :w! /tmp/xxx' . i . '<CR>'
  exec 'nnoremap <Leader>' . i . 'rt :read /tmp/xxx' . i . '<CR>'
endfor


" Open and Source Local RCFile {{{
function! s:open_local_rcfile()
  if !exists("g:vimine_local_rcfile")
    let g:vimine_local_rcfile = ".local/vimrc.vim"
  endif
  exec "tabnew " . g:vimine_local_rcfile
endfunction

function! s:source_local_rcfile()
  if exists("g:vimine_local_rcfile")
    exec "source " . g:vimine_local_rcfile
  else
    echoerr "No local vimrc file .local/vimrc.vim"
  endif
endfunction

command! L42LocalRcFileEdit call <SID>open_local_rcfile()
command! L42LocalRcFileSource call <SID>source_local_rcfile()
" }}}
"
function! s:addApacheSPXLicenceIdentifier() " {{{{{
  let l:identifier = printf(&commentstring, 'SPDX-License-Identifier: Apache-2.0')
  let l:candidate = getline('$')
  if l:candidate == l:identifier
    return
  endif
  call append('$', l:identifier)
endfunction " }}}}}
command! AddApacheSPXLicenceIdentifier call <SID>addApacheSPXLicenceIdentifier()


function! s:mkSecurePassword() " {{{{{
  let l:secure_password = system("mk_secure_password -n")
  exec 'normal a' . l:secure_password
endfunction
command! L42MkSecurePassword call <SID>mkSecurePassword()
inoremap <Leader>sp <Esc>:L42MkSecurePassword<CR>
" }}}}}

if has('lua')
  lua api=require'nvimapi'
endif


function! s:openNotes()
  let l:file = expand("$PERSONAL_NOTES")
  exec "tabnew " . l:file
endfunction

command! OpenNotes call <SID>openNotes()
"
"  Window navigation {{{
"  -----------------
map <Leader>h <C-w>h
map <Leader>j <C-w>j
map <Leader>k <C-w>k
map <Leader>l <C-w>l
map <Leader>p :tabprev<CR>
map <Leader>n :tabnext<CR>
" }}}
" Reloading {{{
"
function! s:forceReload(file)
  let g:vimine_force_reload = 1
  " let l:file = g:vimine_home . "/" . a:file
  exec 'source ' . a:file
  let g:vimine_force_reload = 0
endfunction

function! s:force_reload_completions(A, L, P)
  let l:candidates = expand(g:vimine_home . "/**/*.vim")
  return l:candidates
endfunction
let g:vimine_force_reload = 0
command! -nargs=1 -complete=custom,<SID>force_reload_completions L42ForceReload call <SID>forceReload(<q-args>)
" }}}

" Github {{{
function! s:getUrl(blob)
  let l:base = system('git remote -v | awk ''NR==1{ gsub(".*:", ""); printf $1}''')
  let l:part = substitute(l:base, '\.git$', '', '')
  let l:blob = substitute(a:blob, '.*\.git/', '/blob', '')

  return 'https://github.com/' . l:part . l:blob
endfunction

function! s:openBrowser()
  let l:blob = expand("%")
  let l:url = s:getUrl(l:blob)
  call system('xdg-open ' . l:url)
endfunction

command! Github call <SID>openBrowser()

" " Statusline {{{{
" function! s:gitBranch()
"   return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
" endfunction

" function! StatuslineGit()
"   let l:branchname = s:gitBranch()
"   return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
" endfunction

" function! s:grep(list, expr) " {{{{{
"   let l:list = copy(a:list)
"   return filter(l:list, "match(v:val, '" . a:expr . "') >= 0") 
" endfunction " }}}}}

" function! GitCleanStatus()
"   let l:status = system("git status --porcelain 2>/dev/null" )
"   let l:status_lines = split(l:status, "\n")

"   let l:staged    = !empty(s:grep(l:status_lines, "^[AMRD]."))
"   let l:unmerged  = !empty(s:grep(l:status_lines, "^UU "))
"   let l:unstaged  = !empty(s:grep(l:status_lines, "^.[MTD]"))
"   let l:untracked = !empty(s:grep(l:status_lines, "^\?\? "))
"   let l:clean     = !l:staged && !l:unmerged && !l:unstaged && !l:untracked

"   let l:status_string = join([
"         \ (l:clean?"✓":"✘"), 
"         \ (l:staged?"+":""),
"         \ (l:unmerged?"!":""),
"         \ (l:unstaged?"●":""),
"         \ (l:untracked?"✖":""),
"         \ ])

"   return l:status_string
" endfunction
" " }}}}
" set statusline=%<%f\ %h%m%r%{StatuslineGit()}%=%-14.(%l,%c,%V%)\ %P
set statusline=%<%f\ %h%m%r%=%-14.(%l,%c,%V%)\ %P
" set statusline=%<%f\ %h%m%r%{FugitiveStatusline()}%=%-14.(%l,%c,%V%)\ %P
" }}}
" SPDX-License-Identifier: Apache-2.0
