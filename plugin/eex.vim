
function! s:eex(line1, line2)
  let l:lines = getline(a:line1, a:line2)
  call writefile(l:lines, "/tmp/eex.source")
  call append(a:line2, "")
  exec (a:line2 + 1) . '!elixir -e "IO.puts EEx.eval_file(~s{/tmp/eex.source})"'
  exec "Cleanup"
endfunction

command! -range EEx call <SID>eex(<line1>, <line2>)
