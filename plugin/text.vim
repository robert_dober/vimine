if exists("g:vimine_text_loaded")

  if !g:vimine_force_reload
    finish
  else
    echo "Reload forced text.vim"
  endif
endif

let g:vimine_text_loaded = 1
"
"  Copy From Above
"
function! s:copy_from_above()
  let [_, l:lnb, l:col, _] = getpos('.')
  if l:lnb < 2
    throw "Cannot invoke in first line or empty buffer"
  endif
  let [l:before, l:this] = getline(l:lnb-1, l:lnb)
  if l:col > len(l:before)
    throw "Line above too short"
  endif
  let l:post = strpart(l:before, l:col-1)
  call setline(l:lnb, strpart(l:this, 0, l:col-1) . l:post) 
endfunction

command! L42CopyFromAbove :call <SID>copy_from_above()
" map <Leader>c :L42CopyFromAbove<CR>
" imap <Leader>c <Esc>:L42CopyFromAbove<CR>

map <Leader>i viWS`A
imap <Leader>i <Esc>viWS`A
imap <Leader>: \
imap <Leader>; `
" Commentary fix to go to end of the selection {{{
"
function! s:commentary(line1, line2)
  exec a:line1 . ',' . a:line2 . ' Commentary'
  exec 'normal ' . a:line2 . 'G'
endfunction

command! -range L42Commentary :call <SID>commentary(<line1>, <line2>)
map <Leader>co :L42Commentary<CR>

command! -range=% -nargs=1 SortSections :lua require"selection.sort".sort_by_header(<args>, <line1>, <line2>)

"
" Unicode
"
function! s:insert_utf8(start)
  let l:rb_pgm = '.!ruby -e ''' .
        \ 'puts "      " + (0..15).map {format("\\%02x", _1)}.join(" ");' .
        \ 'puts (0x' . a:start . '..0x' . a:start . ' + 255).each_slice(16).map{ |slice|' .
        \ 'format("\\%0x: ", slice.first) + slice.map{ _1.chr(Encoding::UTF_8) }.join("  ")' .
        \ '}.join("\n")' .
        \ ''''
  " echo l:rb_pgm
  exec l:rb_pgm
endfunction

command! -nargs=1 InsertUtf8 :call <SID>insert_utf8(<q-args>)
"SPDX-License-Identifier: Apache-2.0
