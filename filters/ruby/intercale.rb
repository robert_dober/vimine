require_relative 'tools/monad'
require_relative 'tools/list'

module Intercale extend self
  def run(args)
    Monad.run { |lines| List.intercale(lines, *args) }
  end
end

Intercale.run(ARGV)
