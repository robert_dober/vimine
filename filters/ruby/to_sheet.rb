require_relative 'tools/monad'
require_relative 'tools/list'

require 'csv'

module ToSheet extend self
  SPLITTER = /\A-{3,}\z/
  def run(args)
    Monad.with_side_effect { to_sheet(_1, *args) }
  end

  private

  def to_sheet lines, filename=nil
    filename ||= File.join(ENV.fetch("HOME", "/tmp"), "sheet.csv")

    rows = List.split(lines, SPLITTER)
    File.open(filename, "w") do |fh|
      rows.each do |row|
        p(row:)
        fh.puts(convert(row))
      end
    end
  end

  def convert line
    case line
    when Array
      line.to_csv
    else
      line.split.to_csv
    end
  end
end

ToSheet.run(ARGV)
