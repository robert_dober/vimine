# frozen_string_literal: true

require 'tools/list'
RSpec.describe List do
  describe "product" do
    context "empty" do
      it "works for two" do
        expect(product([[], []])).to be_empty
      end
      it "works for more" do
        expect(product([[], [], [], []])).to be_empty
      end
      it "one empty rules them all" do
        expect(product([[], [], [1]])).to be_empty
      end
    end

    context "more" do
      it "creates n x m" do
        expect(product([[1, 2], [3, 4, 5]]))
          .to eq([
            [1, 3], [1, 4], [1, 5],
            [2, 3], [2, 4], [2, 5],
          ])
      end

      it "creates n x m x l x k" do
        input = [
          %w[a b],
          [1],
          [2, 3, 4],
          [true, false]
        ]
        expected = [
          ["a", 1, 2, true], ["a", 1, 2, false],
          ["a", 1, 3, true], ["a", 1, 3, false],
          ["a", 1, 4, true], ["a", 1, 4, false],
          ["b", 1, 2, true], ["b", 1, 2, false],
          ["b", 1, 3, true], ["b", 1, 3, false],
          ["b", 1, 4, true], ["b", 1, 4, false],
        ]
        expect(product(input)).to eq(expected)
      end
    end
  end

  private

  def product(list); List.product(list) end
end
# SPDX-License-Identifier: Apache-2.0
