# frozen_string_literal: true

require 'tools/list'
RSpec.describe List do
  context "split" do
    describe "edge cases" do
      it "empty" do
        expect(split([], "---")).to eq([[]])
      end
      it "w/o a seperator" do
        list = %w[a b c]
        expect(split(list){ false }).to eq([list])
      end
    end
  end

  context "use case for to_sheet" do
    describe "craete users" do
      let :lines do
        [
          "Create Users for bnm",
          "-------",
          "name",
          "email",
          "admin",
          "------",
          "name 1",
          "email 1",
          "true",
          "------",
          "Create Users for bro",
          "-------",
          "name",
          "email",
          "admin",
          "------",
          "------",
        ]
      end
      let :expected do
        [
          [ "Create Users for bnm" ],
          [  "name", "email", "admin" ],
          [ "name 1", "email 1", "true"] ,
          [ "Create Users for bro" ],
          ["name", "email", "admin"],
          [],
          []
        ]
      end
      let(:splitter) { /-{3,}/ }

      it "converts" do
        expect(split(lines, splitter)).to eq(expected)
      end
    end
  end

  private

  def split(list, splitter=nil, &blk); List.split(list, splitter, &blk) end
end
# SPDX-License-Identifier: Apache-2.0
