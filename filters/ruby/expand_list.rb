require_relative 'tools/monad'

module ExpandList extend self
  def run(*)
    Monad.run { |lines| lines.flat_map(&:split) }
  end
end

ExpandList.run(ARGV)
