require_relative 'tools/monad'
require_relative 'tools/list'

module ExpandList extend self
  def run(args)
    Monad.run{ carthesian(_1, *args) }
  end

  def carthesian lines, joiner=" "
    lists = List.split(lines, %r/-{3,}/)
    prod = List.product(lists)
    prod.map{ _1.join(joiner) }
  end
end

ExpandList.run(ARGV)
