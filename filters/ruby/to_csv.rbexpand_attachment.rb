require_relative 'tools/monad'

module ExpandList extend self
  def run(args)
    Monad.with_side_effect { to_csv(_1, *args) }
  end

  private

  def to_csv lines, filename=nil
    filename ||= File.join(ENV.fetch("TMP", "/tmp"), "xxx.csv")
    File.open(filename, "w") do |fh|
      lines.each { |line| fh.puts(convert(line)) }
    end
  end

  def convert line
    case line
    when Array
      line.to_csv
    else
      line.split.to_csv
    end
  end
end

ExpandList.run(ARGV)
