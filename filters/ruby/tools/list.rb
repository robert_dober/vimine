module List extend self
  def intercale(lines, with="")
    lines.flat_map{ [with, _1] }.drop 1
  end

  def product(lists)
    lists.inject do |l, r|
      l.flat_map do |x|
        r.map { |y| [*x, y] }
      end
    end
  end

  def split(list, splitter=nil, &blk)
    raise ArgumentError, "must not provide splitter and &blk" if splitter && blk

    list.inject([[]]) do |result, element|
      if is_split?(element, splitter, &blk)
        result << []
      else
        result.last << element
        result
      end
    end
  end

  private

  def is_split?(element, splitter, &blk)
    splitter ? splitter === element : blk.(element)
  end
end
# SPDX-License-Identifier: Apache-2.0
