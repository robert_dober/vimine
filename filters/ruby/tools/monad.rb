module Monad extend self
  def map(&blk)
    puts lines.map(&blk)
  end
  def reduce(initial, &blk)
    puts lines.reduce(initial, &blk)
  end
  def run(&blk)
    puts blk.(lines)
  end
  def with_side_effect(&blk)
    blk.(lines)
    puts lines
  end

  private

  def lines
    @__lines__ ||= $stdin.readlines(chomp: true)
  end
end

