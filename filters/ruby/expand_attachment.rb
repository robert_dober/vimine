require_relative 'tools/monad'

module ExpandAttachment extend self
  def run(args)
    Monad.run { expand_attachment(_1, *args) }
  end

  private

  def columns_for(table, attachment_name)
     [
       {name: "file_name", type: "string"},
       {name: "content_type", type: "string"},
       {name: "file_size", type: "integer"},
       {name: "updated_at", type: "datetime"},
     ].map {
       "    add_column :#{table}, :#{attachment_name}_#{_1[:name]}, :#{_1[:type]}"
     }
  end

  def expand_attachment( lines, *)
    lines.first.split => [table, attachment_name, *]
      [
        "  def change",
        *columns_for(table, attachment_name),
        "  end"
      ]
  end
end

ExpandAttachment.run(ARGV)
