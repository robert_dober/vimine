-- local dbg = require("debugger")
-- dbg.auto_where = 2
local H = require "cccomplete.helpers"()

local function complete_lines(ctxt)
  if string.match(ctxt.line, "^%s*end%s*$") then
    return H.make_return_object{
      lines = {string.gsub(ctxt.line, "end%s*$", "<%% end %%>")}
    }
  end
  return require("cccomplete.html")().complete(ctxt)
end


local function complete_with(ctxt, prefix, suffix)
  local suffix = suffix or "%>"
  local new_line = ctxt.prefix .. prefix .. "  " .. suffix .. ctxt.suffix
  return H.make_return_object{
    lines = {new_line},
    offset = 0,
    col = ctxt.col + string.len(prefix)
  }
end

local function complete(ctxt)
  if ctxt.char == "=" then
    return complete_with(ctxt, "<%=")
  end
  if ctxt.char == "%" then
    return complete_with(ctxt, "<%")
  end
  if ctxt.char == "!" then
    return complete_with(ctxt, "<!--", "-->")
  end
  return complete_lines(ctxt)
end

return function()
  return {
    complete = complete
  }
end
