-- local dbg = require("debugger")
-- dbg.auto_where = 2
local A = require "tools.active_support"
local T = require "tools"()
local H = require "cccomplete.helpers"()
local S = require "tools.string"
local F = require "tools.fn"
local L = require "tools.list"
local equals = require'tools.functors'.equals


local function _upt_to_dir(path, dir)
  local segments = S.split(path, "/")
  local tail     = L.tail_from(segments, equals(dir))
  return table.concat(tail, "/")
end

-- local function defmodule_completion(ctxt)
--   if string.match(ctxt.line, "^module%s*$") then
--     local path = _upt_to_lib(ctxt.file_path)
--     local lines = {
--       "defmodule " .. A.camelize_path(path) .. " do",
--       '  @moduledoc ~S"""',
--       '  ',
--       '  """',
--       '',
--       "end"
--     }
--     return H.make_return_object{lines = lines, offset = 1}
--   end
-- end

local function spec_module_completion(ctxt)
  if string.match(ctxt.line, "^module%s*$") then
    local path = _upt_to_dir(ctxt.file_path, "test")
    local lines = {
      "module " .. A.camelize_path(path),
      "    (spec",
      "    ) where",
      "",
      "import Test.Hspec",
      "",
      "spec :: Spec",
      "spec = do",
      "    describe "
    }
    return H.make_return_object{lines = lines, offset = 8}
  end
end

local function example_completion(ctxt)
  if string.match(ctxt.line, "^%s*describe") or
     string.match(ctxt.line, "^%s*it") then
     return H.complete_with_suffix(ctxt, "$ do", 4)
   end
end

local function complete_special(ctxt)
  local file_name = ctxt.file_name
  if file_name then
    if string.match(file_name, "Spec[.]hs$") then
      local completion = spec_module_completion(ctxt)
      if completion then
        return completion
      end
      completion = example_completion(ctxt)
      if completion then
        return completion
      end
    --   return defmodule_completion(ctxt)
    end
  end
end

return function()

  local suffixes = {
    ["=="] = "`shouldBe` ",
  }

  local function complete(ctxt)
    local suffix_completion = H.replace_suffix(suffixes, ctxt)
    if suffix_completion then
      return suffix_completion
    end
    local special_completion = complete_special(ctxt)
    if special_completion then
      return special_completion
    end
    -- local manual_completion = complete_manual(ctxt)
    -- if manual_completion then
    --   return manual_completion
    -- end
    -- local completion = H.complete_from_patterns(ctxt, all_patterns)
    -- -- print("Elixir completion:" .. vim.inspect(completion))
    -- if completion then
    --   return completion
    -- end
    -- completion = H.complete_with_default(ctxt)
    -- if completion then
    --  -- print("Elixir completion:" .. vim.inspect(completion))
    --   return completion
    -- end
    return H.complete_with_do(ctxt)
  end

  return {
    complete = complete
  }
end
