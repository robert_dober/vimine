-- local dbg = require("debugger")
-- dbg.auto_where = 2
local T = require "tools"()

local shortcut_trigger = "%.%.%."

local aligned_parens = {
  {key = "%(%[%s*$", value = "])"},
  {key = "%(%{%s*$", value = "})"},
  {key = "%(%s*$", value = ")"},
  {key = "%{%s*$", value = "}"},
  {key = "%[%s*$", value = "]"},
}

return function()
  local function indent(line, suffix)
    local suffix = suffix or ""
    return string.match(line, "^%s*") .. suffix
  end

  local function make_return_object(opts)
    return {
      lines = opts.lines or {},
      offset = opts.offset or 1,
      col = opts.col or 999
    }
  end

  local function complete_from_patterns(ctxt, all_patterns, alternative)
    for _, current_patterns in ipairs(all_patterns) do
      local fn_completer = T.access_by_match(ctxt.line, current_patterns)
      if fn_completer then return fn_completer(ctxt) end
    end
    if alternative then return alternative(ctxt) end
  end

  local function complete_with_custom(line, custom, indnt)
    local indnt = indnt or 2
    local lines = {
        line,
        indent(line, string.rep(" ", indnt)),
        indent(line, custom)
      }
    return make_return_object{ lines=lines }
  end

  local function complete_with_replacement(line, replacement)
    local fst_line = string.gsub(line, "%s*$", "")
    local snd_line = indent(line, "  ")
    local trd_line = indent(line, replacement.value)

    return make_return_object{lines = {fst_line, snd_line, trd_line}}
  end

  local function complete_with_default(ctxt)
    local replacement = T.key_value_by_match(ctxt.line, aligned_parens)
    if replacement then
      local completion = complete_with_replacement(ctxt.line, replacement)
      if completion then
        return completion
      end
    end
  end

  local function complete_with_suffix(ctxt, suffix, indnt)
    local suffix = suffix or "do"
    local indnt = indnt or 2
    local line = string.gsub(ctxt.line, "%s+" .. suffix .. "%s*$", "")
    line = string.gsub(line, "%s+$", "")
    local lines = {
        line .. " " .. suffix,
        indent(line, string.rep(" ", indnt)),
    }
    return make_return_object{lines=lines}
  end

  local function complete_with_do(ctxt, indnt)
    local indnt = indnt or 2
    local line = string.gsub(ctxt.line, "%s+do%s*$", "")
    line = string.gsub(line, "%s+$", "")
    local lines = {
        line .. " do",
        indent(line, string.rep(" ", indnt)),
        indent(line, "end")
    }
    return make_return_object{lines=lines}
  end

  local function complete_with_end(line, options)
    local options = options or {}
    local suffix = options.suffix or ""
    return complete_with_custom(line, "end" .. suffix, options.indnt)
  end


  local function nop(line)
    return make_return_object{lines={line}, offset=0}
  end

  local function replace_shortcut(shortcuts, ctxt)
    for shortcut, replacement in pairs(shortcuts) do
      shortcut_ = shortcut .. shortcut_trigger
      if string.match(ctxt.line, shortcut_) then
        lines =string.gsub(ctxt.line, shortcut_, replacement)
        return make_return_object{lines={lines}, offset=0}
      end
    end
  end

  local function replace_suffix(suffixes, ctxt)
    for suffix, replacement in pairs(suffixes) do
      local pattern = suffix .. "%s*$"
      if string.match(ctxt.line, pattern) then
        lines =string.gsub(ctxt.line, pattern, replacement)
        return make_return_object{lines={lines}, offset=0}
      end
    end
  end

  return {
    complete_from_patterns = complete_from_patterns,
    complete_with_custom = complete_with_custom,
    complete_with_default = complete_with_default,
    complete_with_do = complete_with_do,
    complete_with_end = complete_with_end,
    complete_with_suffix = complete_with_suffix,
    indent = indent,
    match_any_of = require("tools")().match_any_of,
    make_return_object = make_return_object,
    nop    = nop,
    replace_shortcut = replace_shortcut,
    replace_suffix = replace_suffix,
  }
end

-- SPDX-License-Identifier: Apache-2.0
