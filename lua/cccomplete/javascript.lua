-- local dbg = require("debugger")
-- dbg.auto_where = 2
local A = require "tools.active_support"
local T = require "tools"()
local H = require "cccomplete.helpers"()
local S = require "tools.string"
local F = require "tools.fn"
local L = require "tools.list"
local equals = require'tools.functors'.equals

local mocha_fn_pattern = "^%s*([%l_]+)[(]?"
local mocha_fn_names = {
  add = true,
  context = true,
  describe = true,

  it = true,
  suite = true
}
local mocha_single_names = {
  before_each = true,
  setup = true,
  teardown = true,
}


local function complete_mocha(line)
  local open_paren = "^(%s*[%l_]+)%s"
  line = string.gsub(line, open_paren, "%1(")
  line = string.gsub(line, "[,)]?%s*$", "")

  return H.complete_with_custom(line .. ", () => {", "})")
end

local function maybe_complete_mocha(line)
  local match = string.match(line, mocha_fn_pattern)
  if not match then return nil end
  if mocha_fn_names[match] then
    -- print("mocha fn", match, line)
    return complete_mocha(line)
  end
end

local function mocha_completion(ctxt)
  if string.match(ctxt.line, "^mocha%s*$") then
    local lines = {
      'import mocha_ from "mocha"',
      'const {mocha} = mocha_',
      'import chai_ from "chai"',
      'const expect = chai_.expect',
      ''
    }
    return H.make_return_object{lines = lines, offset = 4}
  end
end
local function complete_special(ctxt)
  return mocha_completion(ctxt)
end

return function()
  local function complete(ctxt)
    -- local shortcut_completion = H.replace_shortcut(shortcuts, ctxt)
    -- if shortcut_completion then
    --   return shortcut_completion
    -- end
    local special_completion = complete_special(ctxt)
    if special_completion then
      return special_completion
    end
    local completion = maybe_complete_mocha(ctxt.line)
    if completion then
      return completion
    end
    -- local manual_completion = complete_manual(ctxt)
    -- if manual_completion then
    --   return manual_completion
    -- end
    -- local completion = H.complete_from_patterns(ctxt, all_patterns)
    -- -- print("Javascript completion:" .. vim.inspect(completion))
    -- if completion then
    --   return completion
    -- else
    --   return H.complete_with_do(ctxt)
    -- end
    return H.complete_with_custom(ctxt.line .. " {", "}")
  end

  return {
    complete = complete
  }
end
