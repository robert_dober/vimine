-- local dbg = require("debugger")
-- dbg.auto_where = 2
local indent = require "cccomplete.helpers"().indent
local complete_with_do = require "cccomplete.helpers"().complete_with_do
local complete_with_end = require "cccomplete.helpers"().complete_with_end
local H = require "cccomplete.helpers"()

local ruby_no_do = "^%s*(%a+)"
local ruby_no_dos = {
  module = true,
  class = true,
  def = true,
}


local function special_completer(ctxt)
  if ctxt.lnb == 1 and string.match(ctxt.line, "^%s*$") then
    return H.make_return_object{lines = {"# frozen_string_literal: true", ""}}
  end
  if string.match(ctxt.line, "^%s*let") then
    local completion = require'cccomplete.ruby.complete_let'(ctxt)
    if completion then
      return H.make_return_object(completion)
    end
  end
  if string.match(ctxt.line, "^%s*memo%s+") then
    return require'cccomplete.ruby.complete_memo'(ctxt)
  end
  local hash_key_conversion = require'cccomplete.ruby.complete_hash_keys'(ctxt)
  if hash_key_conversion then
    return hash_key_conversion
  end
end

local function complete(ctxt)
  local line = ctxt.line
  local special_completion = special_completer(ctxt)
  if special_completion then
    return special_completion
  end
  local no_do = ruby_no_dos[string.match(line, ruby_no_do)]
  if no_do then
    return complete_with_end(line)
  else
    return complete_with_do(ctxt)
  end
end

return function()
  return {
    complete = complete
  }
end
