-- local dbg = require("debugger")
-- dbg.auto_where = 2

return function(ctxt)
  local _, _, prefix, name = string.find(ctxt.line, "^(%s*)let%s[:]?([%w_%d]+)%s*$")
  if name then
    local line = prefix .. 'let(:' .. name .. ') {  }'
    return {lines = {line}, col = string.len(line) - 3, offset = 0}
  end
end
