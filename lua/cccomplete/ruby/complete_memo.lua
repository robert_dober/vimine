-- local dbg = require("debugger")
-- dbg.auto_where = 2
local indent = require "cccomplete.helpers"().indent
local complete_with_end = require "cccomplete.helpers"().complete_with_end
local H = require "cccomplete.helpers"()

return function(ctxt)
  local _, _, prefix, name = string.find(ctxt.line, "^(%s*)memo%s+([%w_%d]+)%s*$")
  if name then
    local lines = {prefix .. "def " .. name}
    table.insert(lines, prefix .. "  @__" .. name .. "__ ||=" )
    table.insert(lines, prefix .. "    ")
    table.insert(lines, prefix .. "end")

    return {lines = lines, col = 999, offset = 2}
  end
end
