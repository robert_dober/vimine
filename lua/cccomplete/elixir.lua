-- local dbg = require("debugger")
-- dbg.auto_where = 2
local A = require "tools.active_support"
local T = require "tools"()
local H = require "cccomplete.helpers"()
local S = require "tools.string"
local F = require "tools.fn"
local L = require "tools.list"
local equals = require'tools.functors'.equals

local shortcuts = {
  ["iex"] = "iex>",
  ["@"]   = "|>",
}

local function _upt_to_lib(path)
  local segments = S.split(path, "/")
  local tail     = L.tail_from(segments, equals("lib"))
  return table.concat(tail, "/")
end

local function defmodule_completion(ctxt)
  if string.match(ctxt.line, "^module%s*$") then
    local path = _upt_to_lib(ctxt.file_path)
    local lines = {
      "defmodule " .. A.camelize_path(path) .. " do",
      '  @moduledoc ~S"""',
      '  ',
      '  """',
      '',
      "end"
    }
    return H.make_return_object{lines = lines, offset = 1}
  end
end

local function test_completion(ctxt)
  if string.match(ctxt.line, "^module%s*$") then
    local usage_macro = ctxt.api.read_var("g:usage_macro", "ExUnit.Case")
    local lines = {
      "defmodule " .. A.camelize_path(ctxt.file_path) .. " do",
      "  use " .. usage_macro,
      "",
      "  ",
      "end"
    }
    return H.make_return_object{lines = lines, offset = 3}
  end
end

local function complete_special(ctxt)
  local file_name = ctxt.file_name
  if not file_name then
    return nil
  end
  if string.match(file_name, "_test[.]exs$") then
    return test_completion(ctxt)
  else
    return defmodule_completion(ctxt)
  end
end

local function complete_fence(ctxt)
  local line = ctxt.line

  return H.make_return_object{
    lines = {ctxt.line, H.indent(line, ""), H.indent(line, "```")},
    offset=1}
end

local function fn_complete_first_docstring(with)
  local with = with or ""
  return function(ctxt)
    local line = string.gsub(ctxt.line, ">%s*", with .. "> ")
    return H.make_return_object{lines = {line}, offset = 0}
  end
end
local function fn_complete_docstring(with)
  local with = with or ""
  return function(ctxt)
    local line = string.gsub(ctxt.line, ">", with .. ">")
    local number = string.match(line, "([(]%d+[)])")
    return H.make_return_object{lines = {line, H.indent(line) .. "..." .. number .. "> "}}
  end
end
local doctest_patterns = {
  ["^%s*```"] = complete_fence,
  ["^%s%s%s%s+iex>"] = fn_complete_first_docstring("(0)"),
  ["^%s%s%s%s+[.][.][.]>"] = fn_complete_docstring("(0)"),
  ["^%s%s%s%s+iex[(]%d+[)]>"] = fn_complete_docstring(),
  ["^%s%s%s%s+[.][.][.][(]%d+[)]>"] = fn_complete_docstring(),
}

local function fn_continue_pipe(ctxt)
  return H.make_return_object{lines = {ctxt.line, H.indent(ctxt.line, "|> ")}}
end
local function fn_first_pipe(ctxt)
  local line = string.gsub(ctxt.line, "%s*>%s*$", "")
  return H.make_return_object{lines = {line, H.indent(line, "|> ")}}
end

local pipe_patterns = {
  ["%s>%s*$"] = fn_first_pipe,
  ["^%s*|>%s"] = fn_continue_pipe
}

local function fn_doctest(ctxt)
  return H.make_return_object{lines = {ctxt.line, H.indent(ctxt.line), H.indent(ctxt.line, '"""')}}
end
local function fn_pure_docstring(ctxt)
  local doctype = "@" .. string.match(ctxt.line, '^%s*(%w+)$')
  return H.make_return_object{lines = {H.indent(ctxt.line, doctype .. ' ~S"""'), H.indent(ctxt.line), H.indent(ctxt.line, '"""')}}
end
local docstring_patterns = {
  ['^%s*doc$'] = fn_pure_docstring,
  ['^%s*moduledoc$'] = fn_pure_docstring,
  ['^%s*@doc%s+"""'] = fn_doctest,
  ['^%s*@moduledoc%s+"""'] = fn_doctest,
}

local function spec_complete(ctxt)
  local fun_name = string.match(ctxt.post_line, "^%s*defp?%s([%w_?!]+)")
  if fun_name then
    local line = H.indent(ctxt.post_line) .. "@spec " .. fun_name .. "("
    return H.make_return_object{lines = {line}, offset = 0}
  end
end
local spec_patterns = {
  ['^%s*spec'] = spec_complete,
}
local all_patterns = {
  doctest_patterns, docstring_patterns, pipe_patterns, spec_patterns
}

local function complete_manual(ctxt)
  if string.match(ctxt.line, "%(.*fn.*->") then
      return H.complete_with_custom(S.rtrim(ctxt.line), "end)")
  elseif string.match(ctxt.line, ".*fn.*->") then
      return H.complete_with_custom(S.rtrim(ctxt.line), "end")
  end
end
return function()
  local function complete(ctxt)
    local shortcut_completion = H.replace_shortcut(shortcuts, ctxt)
    if shortcut_completion then
      return shortcut_completion
    end
    local special_completion = complete_special(ctxt)
    if special_completion then
      return special_completion
    end
    local manual_completion = complete_manual(ctxt)
    if manual_completion then
      return manual_completion
    end
    local completion = H.complete_from_patterns(ctxt, all_patterns)
    -- print("Elixir completion:" .. vim.inspect(completion))
    if completion then
      return completion
    end
    completion = H.complete_with_default(ctxt)
    if completion then
     -- print("Elixir completion:" .. vim.inspect(completion))
      return completion
    end
    return H.complete_with_do(ctxt)
  end

  return {
    complete = complete
  }
end
