-- local dbg = require("debugger")
-- dbg.auto_where = 2
local indent = require "cccomplete.helpers"().indent
-- local complete_with_do = require "cccomplete.helpers"().complete_with_do
-- local complete_with_end = require "cccomplete.helpers"().complete_with_end
local H = require "cccomplete.helpers"()

local html_template_lines = {
  '<!DOCTYPE html>',
  '<html>',
  '  <head>',
  '    <title>INSERT TITLE HERE</title>',
  '    <meta charset="UTF-8">',
  '    <meta name="viewport" content="width=device-width, initial-scale=1">',
  '    <!-- <link rel="stylesheet" href="INSERT URI HERE"> -->',
  '  </head>',
  '  <body>',
  '    ', -- row #10
  '  </body>',
  '</html>'
}

local function maybe_complete_first_line(ctxt)
  if ctxt.lnb > 1 then
    return nil
  end
  if string.match(ctxt.line, "^%s*$") then
    return H.make_return_object{
      lines = html_template_lines,
      offset = 9
    }
  end
end

local function complete(ctxt)
  local first_line_completion = maybe_complete_first_line(ctxt)
  if first_line_completion then
    return first_line_completion
  end

  local line = ctxt.line
  local _, _, tag, closing = string.find(line, "^%s*<(%a+)(.*)%s*$")
  if tag then
    local line = string.gsub(line, "[>]%s*$", "")
    line = string.gsub(line, "$", ">")
    local new_line = H.indent(line, "  ")
    local end_line = H.indent(line, "</" .. tag .. ">")
    return H.make_return_object{
      lines = {line, new_line, end_line}
    }
  end
end

return function()
  return {
    complete = complete
  }
end
