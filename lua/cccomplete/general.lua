-- local dbg = require("debugger")
-- dbg.auto_where = 2
local H = require'cccomplete.helpers'()
local ctxt


local completers = {
  crystal  = require("cccomplete.crystal")(),
  eruby   = require("cccomplete.eruby")(),
  elixir   = require("cccomplete.elixir")(),
  haskell  = require("cccomplete.haskell")(),
  html     = require("cccomplete.html")(),
  javascript  = require("cccomplete.javascript")(),
  lua      = require("cccomplete.lua")(),
  markdown = require("cccomplete.markdown")(),
  ruby     = require("cccomplete.ruby")(),
  rust     = require("cccomplete.rust")(),
}

local function _insert_code_block()
  local fl = string.gsub(ctxt.line, "%%code%s*", "```")
  return H.make_return_object{lines = { fl, "", "```" }}
end
local function _insert_date()
  local now = ctxt.api.eval('strftime("%F", localtime())')
  local nl  = string.gsub(ctxt.line, "%%date", now)
  return H.make_return_object{lines = {nl}, offset = 0}
end
local function _insert_datetime()
  local now = ctxt.api.eval('strftime("%F %T", localtime())')
  local nl  = string.gsub(ctxt.line, "%%datetimez", now .. "Z")
  local nl  = string.gsub(nl, "%%datetime", now)
  return H.make_return_object{lines = {nl}, offset = 0}
end
local function _insert_time()
  local now = ctxt.api.eval('strftime("%T", localtime())')
  local nl  = string.gsub(ctxt.line, "%%time", now)
  return H.make_return_object{lines = {nl}, offset = 0}
end
local function _insert_literal(trigger, literal)
  return function()
    local replacement_line = string.gsub(ctxt.line, "%%" .. trigger, literal)
    return H.make_return_object{lines = {replacement_line}, offset = 0}
  end
end
local general_patterns = {
  code = _insert_code_block,
  date = _insert_date,
  datetime = _insert_datetime,
  datetimez = _insert_datetime,
  time = _insert_time,
  done = _insert_literal("done", "[✓]"),
  todo = _insert_literal("todo", "[ ]"),
}
local general_pattern = "%%([%w_]+)"
local function check_for_general_completer()
  local m = string.match(ctxt.line, general_pattern)
  return general_patterns[m]
end

local function complete(ct)
  ctxt = ct
  local general_completer = check_for_general_completer()

  if general_completer then
    return general_completer()
  end

  local completer = completers[ctxt.ft]
  if completer then
     return completer.complete(ctxt)
  end

end

return {
  complete = complete,
}

