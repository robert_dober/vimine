local api = vim.api

return function()
  local pos = vim.api.nvim_win_get_cursor(0)
  api.nvim_command("tabnew %")
  api.nvim_win_set_cursor(0, pos)
  vim.lsp.buf.definition()
end
