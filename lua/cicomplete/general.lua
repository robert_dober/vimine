-- local dbg = require("debugger")
-- dbg.auto_where = 2

local function squash_spaces(ctxt)
  local lhs = string.gsub(ctxt.prefix, '%s+$', '')
  local rhs = string.gsub(ctxt.suffix, '^%s+', '')
  return lhs .. ctxt.char .. rhs
end

local chars = {
  [' '] = squash_spaces,
}

return function(ctxt)
  if ctxt.char == " " then
    return squash_spaces(ctxt)
  end
end

-- SPDX-License-Identifier: Apache-2.0
