local parse_args  = require'tools.args'.parse_args
-- local dbg = require("debugger")
-- -- Consider enabling auto_where to make stepping through code easier to follow.
-- dbg.auto_where = 2

describe("edge cases", function()
  it("empty", function()
    assert.are.same({{}, {}}, parse_args(""))
  end)
  it("still empty", function()
    assert.are.same({{}, {}}, parse_args(" "))
  end)
end)
describe("flags", function()
  it("a single one", function()
    assert.are.same({{help=true}, {}}, parse_args(":help"))
  end)
  it("ore more", function()
    assert.are.same({{help=true, verb=true}, {}}, parse_args(":verb :help"))
  end)
end)

describe("keys", function()
  it("one key value pair", function()
    assert.are.same({{n="42"}, {}}, parse_args("n:42"))
  end)
  it("two key value pairs", function()
    assert.are.same({{n="42", hello="world:again"}, {}}, parse_args("n:42 hello:world:again"))
  end)
end)

describe("positionals", function()
  it("one positional", function()
    assert.are.same({{}, {"just-a-value"}}, parse_args("just-a-value"))
  end)
  it("or more", function()
    assert.are.same({{}, {"one", "two", "three"}}, parse_args("one two three"))
  end)
end)

describe("combination of all", function()
  it("has everything", function()
    input = "hello :alpha n:2 world :beta m:3"
    positionals = { "hello", "world" }
    expected = {}
    expected["alpha"] = true
    expected["beta"] = true
    expected["n"] = "2"
    expected["m"] = "3"
    assert.are.same({expected, positionals}, parse_args(input))
  end)

end)
-- SPDX-License-Identifier: Apache-2.0
