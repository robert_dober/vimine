local F = require'tools.functors'

describe("element_fn", function()
  local is_vowel = F.element_fn({"a", "e", "i", "o", "u"})
  it("is true for vowels", function()
    for _, v in pairs({"a", "i", "u", "e", "o"}) do
      assert.is_true(is_vowel(v))
    end
  end)
  it("is not so much true for consonants", function()
    for _, c in pairs({"b", "j", "y", "z"}) do
      assert.is_false(is_vowel(c))
    end
  end)
  it("or others", function()
    for _, o in pairs({"", " ", "1", "'", "a "}) do
      assert.is_false(is_vowel(o))
    end
  end)
end)

describe("negation", function()
  local function is_positive(n)
    if n > 0 then return true else return false end
  end
  local is_negative = F.negate(is_positive)
  it("is true for small numbers", function()
    assert.is_true(is_negative(-1))
  end)
  it("is true for slightly larger numbers", function()
    assert.is_true(is_negative(0))
  end)
  it("is not true for huge numbers", function()
    assert.is_false(is_negative(1))
  end)
end)

