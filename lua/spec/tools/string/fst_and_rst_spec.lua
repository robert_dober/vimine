-- local dbg = require("debugger")
-- dbg.auto_where = 2

local fr = require "tools.string".fst_and_rst

describe("fst_and_rst with default", function()
  it("a typical use case", function()
    assert.are.same({"a", "b"}, fr("ab"))
  end)

  it("edge case empty rst", function()
    assert.are.same({"a", ""}, fr("a"))
  end)

  it("void", function()
    assert.are.same({"", ""}, fr(""))
  end)
end)

describe("fst_and_rst", function()
  it("a typical use case", function()
    assert.are.same({"ab", "c"}, fr("abc", 2))
  end)

  it("edge case empty rst", function()
    assert.are.same({"abc", ""}, fr("abc", 3))
  end)

  it("void", function()
    assert.are.same({"", ""}, fr("", 10))
  end)
end)

-- SPDX-License-Identifier: Apache-2.0
