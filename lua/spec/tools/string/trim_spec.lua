-- local dbg = require("debugger")
-- dbg.auto_where = 2

local rtrim = require "tools.string".rtrim

describe("rtrim", function()
  it("a typical use case", function()
    assert.equal("a", rtrim("a "))
  end)

  it("is harmless", function()
    assert.equal("ab", rtrim("ab"))
  end)
end)
-- SPDX-License-Identifier: Apache-2.0
