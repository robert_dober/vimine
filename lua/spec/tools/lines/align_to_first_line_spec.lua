-- local dbg = require("debugger")
-- dbg.auto_where = 2

local align = require'tools.lines'.align_to_first_line

describe('NOPs', function()
  it('does not change unindented list', function()
    local subject = { "alpha", " beta"}
    local expected = { "alpha\n", " beta\n"}
    assert.are.same(expected, align(subject))
  end)
  it('removes spaces, or more', function()
    local subject = { "  alpha", "  beta", "gamma", "   delta"}
    local expected = { "alpha\n", "beta\n", "mma\n", " delta\n"}
    assert.are.same(expected, align(subject))
  end)
end)
-- SPDX-License-Identifier: Apache-2.0
