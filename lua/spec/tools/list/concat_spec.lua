local concat = require'tools.list'.concat

describe("concat", function()
  describe("with modif", function()
    local head = {"a", "b", "c"}
    local middle = {"1"}
    local tail = {"d", "e", "f"}
    local result = concat(head, middle, tail)
    it("yields result", function()
      assert.are.same({"a", "b", "c", "1","d", "e", "f"}, result)
    end)
    it("did not change any param", function()
      assert.are.same({"a", "b", "c"}, head)
      assert.are.same({"1"}, middle)
      assert.are.same({"d", "e", "f"}, tail)
    end)
  end)
end)
