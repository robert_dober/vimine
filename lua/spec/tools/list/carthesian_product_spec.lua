-- local dbg = require("debugger")
-- dbg.auto_where = 2
local carth = require'tools.list.product'.carthesian_product

describe("carth", function()
  describe("two lists", function()
    it("singletons", function()
      local lists = {{1}, {2}}
      assert.are.same({{1, 2}}, carth(lists))
    end)
    it("more values", function()
      local expected = {
        {1, "a"}, {1, "b"}, {1, "c"},
        {2, "a"}, {2, "b"}, {2, "c"},
      }
      assert.are.same(expected, carth{{1, 2}, {"a", "b", "c"}})
    end)
  end)
  describe("more lists", function()
    it("3 lists", function()
      local tables = {
        {1},
        {20, 30},
        {400, 500, 600}
      }
      local expected = {
        {1, 20, 400}, {1, 20, 500}, {1, 20, 600},
        {1, 30, 400}, {1, 30, 500}, {1, 30, 600},
      }
      assert.are.same(expected, carth(tables))
    end)
    it("4 lists", function()
      local tables = {
        {1, 2},
        {30},
        {400, 500, 600},
        {7000, 8000}
      }
      local expected = {
        {1, 30, 400, 7000}, {1, 30, 400, 8000},
        {1, 30, 500, 7000}, {1, 30, 500, 8000},
        {1, 30, 600, 7000}, {1, 30, 600, 8000},
        {2, 30, 400, 7000}, {2, 30, 400, 8000},
        {2, 30, 500, 7000}, {2, 30, 500, 8000},
        {2, 30, 600, 7000}, {2, 30, 600, 8000},
      }
      assert.are.same(expected, carth(tables))
    end)
  end)
end)
