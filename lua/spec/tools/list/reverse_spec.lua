local reverse = require'tools.list'.reverse

describe("reverse", function()
  it("empty", function()
    assert.are.same({}, reverse{})
  end)
  it("one element", function()
    assert.are.same({"a"}, reverse{"a"})
  end)
  it("two elements", function()
    assert.are.same({2, 1}, reverse{1, 2})
  end)
end)
