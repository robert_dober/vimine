-- local dbg = require("debugger")
-- dbg.auto_where = 2

local P = require'peg/parsers'
local C = require'peg/combinators'

describe("skip", function()
  local skip_ws_parser = C.skip(P.pattern_parser("%s*"))
  it("succeeds on empty", function()
    assert.are.same({"", ""}, skip_ws_parser(""))
  end)
  it("succeeds on no ws", function()
    assert.are.same({"", "as"}, skip_ws_parser("as"))
  end)
  it("succeeds and removes ws", function()
    assert.are.same({"", "as"}, skip_ws_parser("  as"))
  end)
end)

-- SPDX-License-Identifier: Apache-2.0
