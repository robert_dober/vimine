-- local dbg = require("debugger")
-- dbg.auto_where = 2

local P = require'peg/parsers'
local C = require'peg/combinators'

describe("tagged", function()
  local id_parser = C.tagged(C.sequence({P.pattern_parser("[_%a]"), C.many(P.pattern_parser("[_%a%d]"))}), "id")
  it("returns a tagged value", function()
    assert.are.same({{"id", "_a42"},"%"}, id_parser("_a42%"))
  end)
  it("is an NOP on failure", function()
    assert.is_nil(id_parser(""))
  end)
end)

describe("map", function()
  local ws_parser = C.tagged(P.pattern_parser("%s+"), "ws")
  local rest_parser = C.tagged(P.pattern_parser("[a-z]+"), "txt")
  local token_parser = C.choice({ws_parser, rest_parser})
  local parser = C.map(token_parser)
  it("returns a list", function()
    local input = " a b% "
    local expected = {{{"ws", " "}, {"txt", "a"}, {"ws", " "}, {"txt", "b"}}, "% "}
    assert.are.same(expected, parser(input))
  end)

end)

-- SPDX-License-Identifier: Apache-2.0
