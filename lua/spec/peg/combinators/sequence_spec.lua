-- local dbg = require("debugger")
-- dbg.auto_where = 2

local P = require'peg/parsers'
local C = require'peg/combinators'


describe("apply", function()
  local int_parser = C.apply(P.pattern_parser("%d+"), tonumber)
  it("converts the result", function()
    assert.are.same({42, " +"}, int_parser("42 +"))
  end)
  it("does notthing on failure", function()
    assert.is_nil(int_parser("a42"))
  end)
end)

describe("sequence", function()
  local reg_parser = C.sequence({P.char_parser("R"), P.pattern_parser("[0-7]"), P.char_parser(" ")})
  it("can parse a register", function()
    assert.are.same({"R1 ", ""}, reg_parser("R1 "))
  end)
  it("needs a ws after the digit though", function()
    assert.is_nil(reg_parser("R1"))
  end)
  it("is limited to 8 registers", function()
    assert.is_nil(reg_parser("R8 "))
  end)
end)

-- SPDX-License-Identifier: Apache-2.0
