-- local dbg = require("debugger")
-- dbg.auto_where = 2

local P = require'peg/parsers'
local function verify_parser(result, expected, rest, position)
  local position = position or 2
  assert.are.same({expected, {position, rest}}, result)
end

describe("char parser", function()
  local a_parser = P.char_parser("a")

  -- it("can parse a string starting with an `a`", function()
  --   verify_parser(a_parser("a."), "a", ".")
  --   -- assert.are.same({"a", "."}, a_parser("a."))
  -- end)
  it("can parse a string with only an `a`", function()
    assert.are.same({"a", ""}, a_parser("a"))
  end)

  it("will however fail on an empty string", function()
    assert.is_nil(a_parser(""))
  end)
  it("or a string not commencing in`a`", function()
    assert.is_nil(a_parser("ba"))
  end)
end)

describe("pattern_parser → letters", function()
  local letter_parser = P.pattern_parser("%a")
  it("parses a lowercase letter", function()
    assert.are.same({"a", ""}, letter_parser("a"))
  end)
  it("or it pareses uppercase letters with some rest", function()
    assert.are.same({"H", "ello"}, letter_parser("Hello"))
  end)
end)

describe("pattern parser → ranges", function()
  local vowel_parser = P.pattern_parser("[aeiou]")
  it("parses a vowel", function()
    assert.are.same({"e", "llo"}, vowel_parser("ello"))
    assert.are.same({"u", "llo"}, vowel_parser("ullo"))
  end)
  it("however not a consonant", function()
    assert.is_nil(vowel_parser("hello"))
  end)
  it("or empty", function()
    assert.is_nil(vowel_parser(""))
  end)
end)

describe("pattern parsers → for longer substrings", function()
  local leading_ws_parser = P.pattern_parser("%s+")
  it("parses one leading ws", function()
    assert.are.same({" ", "a"}, leading_ws_parser(" a"))
  end)
  it("or two", function()
    assert.are.same({"  ", ""}, leading_ws_parser("  "))
  end)
  it("needs one at least though", function()
    assert.is_nil(leading_ws_parser(""))
  end)
end)

-- SPDX-License-Identifier: Apache-2.0
