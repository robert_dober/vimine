-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local hspec = require'testing.haskell_hspec'
local api = require'nvimapi'

insulate("an `it` line", function()
  stub_vim{lines = {"", 'it "is an example" $ do', ""}, cursor = {2, 3}, ft = "haskell"}
  it("sets a global variable", function()
    hspec.run("__alpha_beta_gamma__")
    assert.is_equal('stack build && stack runghc Spec.hs -- -m "is an example"', api.var("__alpha_beta_gamma__"))
  end)
end)


