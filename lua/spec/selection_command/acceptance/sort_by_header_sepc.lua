-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local api = require'nvimapi'
local sort_by_header = require'selection.sort'.sort_by_header

insulate("some blocks", function()
  local lines = {
    "alpha",
    "Context gamma",
    "1",
    "Context beta",
    "2"
  }
  local expected = {
    "alpha",
    "Context beta",
    "2",
    "Context gamma",
    "1"
  }
  stub_vim{lines = lines}
  it("changes the buffer accordingly", function()
    sort_by_header("Context (.*)", 1, 5)
    assert.are.same(expected, api.lines(1, 5))
  end)
end)

--SPDX-License-Identifier: Apache-2.0
