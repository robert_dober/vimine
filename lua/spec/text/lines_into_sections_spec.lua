-- local dbg = require("debugger")
-- dbg.auto_where = 2
local into_sections = require'tools.lines'.lines_into_sections
local Pair = require'objects/pair'

describe("one section only, no match", function()
  local result = into_sections("no match", {"a", "b", "c"})
  it("has only the before entry", function()
    assert.are.same({Pair:new{left = "", right = {"a", "b", "c"}}}, result)
  end)
end)

describe("two sections", function()
  it("no match, then match", function()
    local result = into_sections("^(%w+):", {"a", "name: b", "c"})
    local expected = {
      Pair:new{left="name", right={"name: b", "c"}},
      Pair:new{left="", right={"a"}}
    }
    assert.are.same(expected, result)
  end)
  it("two matches", function()
    local result = into_sections("^(%w+):", {"id: a", "name: b"})
    local expected = {
      Pair:new{left="name", right={"name: b"}},
      Pair:new{left="id", right={"id: a"}},
      Pair:new{left="", right={}}
    }
    assert.are.same(expected, result)
  end)
end)
--SPDX-License-Identifier: Apache-2.0
