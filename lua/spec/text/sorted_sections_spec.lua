-- local dbg = require("debugger")
-- dbg.auto_where = 2
local sorted_sections = require'tools.lines'.sorted_sections

describe("many sections", function()
  it("no match, then match", function()
    local result = sorted_sections("^(%w+):", {"a", "name: b", "c"})
    local expected = { "a", "name: b", "c" }
    assert.are.same(expected, result)
  end)
  it("two matches", function()
    local result = sorted_sections("^(%w+):", {"id: a", "name: b"})
    local expected =  {"id: a", "name: b"}
    assert.are.same(expected, result)
  end)
  it("three matches, four sections", function()
    local lines =
      { "header", "context: gamma", "", "gamma", "alpha", "context: alpha", " ", "context: beta", "1"}
    local result = sorted_sections("^context: (.*)", lines)
    local expected =
      { "header",  "context: alpha", " ", "context: beta", "1", "context: gamma", "", "gamma", "alpha"}
    assert.are.same(expected, result)
  end)

end)
--SPDX-License-Identifier: Apache-2.0
