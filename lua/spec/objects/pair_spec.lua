-- local dbg = require("debugger")
-- dbg.auto_where = 2

local Pair = require'objects.pair'

describe("construction", function()
  local pair = Pair:new{left = 1, right = 2}
  it("is a table", function()
    assert.is_equal(1, pair.left)
    assert.is_equal(2, pair.right)
  end)
  it("is immutable", function()
    local new = pair:set_left(0)
    assert.is_equal(1, pair.left)
    assert.is_equal(2, pair.right)
    assert.is_equal(0, new.left)
    assert.is_equal(2, new.right)
  end)
  it("has an order function", function()
    local smaller = Pair.new{left = 0, right = 3}
    assert.is_true(Pair.smaller(smaller, pair))
    assert.is_false(Pair.smaller(pair, smaller))
  end)
end)

describe("mutable with set", function()
  local pair = Pair:new{left = 1, right = 2}
  it("can be changed with the set method", function()
    pair:set("left", function(value) return value*2 end)
    assert.are.same({left = 2, right = 2}, pair)
  end)
end)

--SPDX-License-Identifier: Apache-2.0
