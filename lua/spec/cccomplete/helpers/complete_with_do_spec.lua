-- local dbg = require("debugger")
-- dbg.auto_where = 2

local subject = require'cccomplete.helpers'().complete_with_do

local random_string = require 'spec.helpers.random'.random_string

describe('default indent', function()
  it('just adds " do"', function()
    local line = random_string("", 30)
    local result = subject{line = line}
    local expected = {col = 999, offset = 1, lines = {line .. " do", "  ", "end"}}
    assert.are.same(expected, result)
  end)

  it('respects a prefix', function()
    local line = random_string("  ", 30)
    local result = subject{line = line}
    local expected = {col = 999, offset = 1, lines = {line .. " do", "    ", "  end"}}
    assert.are.same(expected, result)
  end)

  it('can have a different indent', function()
    local line = random_string("  ", 30)
    local result = subject({line = line}, 4)
    local expected = {col = 999, offset = 1, lines = {line .. " do", "      ", "  end"}}
    assert.are.same(expected, result)
  end)
end)
