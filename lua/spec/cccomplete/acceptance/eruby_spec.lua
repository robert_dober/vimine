-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local complete = require'cccomplete'.complete
local api = require'nvimapi'

insulate("a %", function()
  --                 0....+.....1
  stub_vim{lines = {"prefix%suffix", ""}, cursor = {1, 6}, ft = "eruby"}
  complete()
  it("replaces with <%  %>", function()
    assert.are.same({"prefix<%  %>suffix"}, api.lines(1, 1))
    assert.are.same({1, 8}, api.cursor())
  end)
end)

insulate("a =", function()
  --                 0....+.....1
  stub_vim{lines = {"prefix=suffix", ""}, cursor = {1, 6}, ft = "eruby"}
  complete()
  it("replaces with <%  %>", function()
    assert.are.same({"prefix<%=  %>suffix"}, api.lines(1, 1))
    assert.are.same({1, 9}, api.cursor())
  end)
end)

insulate("a !", function()
  --                 0....+.....1
  stub_vim{lines = {"prefix!suffix", ""}, cursor = {1, 6}, ft = "eruby"}
  complete()
  it("replaces with <!--  -->", function()
    assert.are.same({"prefix<!--  -->suffix"}, api.lines(1, 1))
    assert.are.same({1, 10}, api.cursor())
  end)
end)

insulate("end", function()
  stub_vim{lines = {"  end  ", ""}, cursor = {1, 6}, ft = "eruby"}
  complete()
  it("replaces with <% end %>", function()
    assert.are.same({"  <% end %>"}, api.lines(1, 1))
    assert.are.same({2, 999}, api.cursor())
  end)
end)

insulate("open close a tag", function()
  stub_vim{lines = {'  <div class="hello"', ""}, cursor = {1, 6}, ft = "eruby"}
  complete()
  it("creates a block", function()
    assert.are.same({'  <div class="hello">', '    ', '  </div>', ''}, api.lines(1, 4))
    assert.are.same({2, 999}, api.cursor())
  end)
end)
insulate("close a tag", function()
  stub_vim{lines = {'<span>  ', ""}, cursor = {1, 6}, ft = "eruby"}
  complete()
  it("creates a block", function()
    assert.are.same({'<span>', '  ', '</span>', ''}, api.lines(1, 4))
    assert.are.same({2, 999}, api.cursor())
  end)
end)
