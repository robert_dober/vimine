-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local complete = require'cccomplete'.complete
local api = require'nvimapi'
local r = require'spec.helpers.random'

local function setup(evaluations)
  stub_vim{
    lines = {"module", "# a comment"},
    cursor = {1, 3},
    ft = "elixir",
    evaluations = evaluations,
    path = "test/amodule/hello_test.exs"}

  complete()
end

insulate("module completion, default macro", function()
  setup{
      { 'exists("g:usage_macro")', 0},
  }
  it("has added and modified the lines", function()
    assert.are.same({
      "defmodule Test.Amodule.HelloTest do",
      "  use ExUnit.Case",
      "",
      "  ",
      "end"}, api.lines(1, 5))
    assert.are.same({4, 999}, api.cursor())
  end)
end)
insulate("module completion, custom macro", function()
  setup{
      { 'exists("g:usage_macro")', 1},
      { 'g:usage_macro', "MyMacro" }
  }
  it("has added and modified the lines", function()
    assert.are.same({
      "defmodule Test.Amodule.HelloTest do",
      "  use MyMacro",
      "",
      "  ",
      "end"}, api.lines(1, 5))
    assert.are.same({4, 999}, api.cursor())
  end)
end)

--SPDX-License-Identifier: Apache-2.0
