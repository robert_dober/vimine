-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local complete = require'cccomplete'.complete
local api = require'nvimapi'
local r = require'spec.helpers.random'

insulate("empty first line → # frozen_string_literal: true", function()
  stub_vim{lines = {"", "# a comment", ""}, cursor = {2, 3}, ft = "ruby"}
  complete()
  it("falls back to do completion if not on the first line", function()
    assert.are.same({"", "# a comment do", "  ", "end", ""}, api.lines(1, 5))
  end)
end)

insulate("empty first line → # frozen_string_literal: true", function()
  stub_vim{lines = {"# a comment", ""}, cursor = {1, 3}, ft = "ruby"}
  complete()
  it("falls back to do completion if line not empty", function()
    assert.are.same({"# a comment do", "  ", "end", ""}, api.lines(1, 4))
  end)
end)

insulate("empty first line → # frozen_string_literal: true", function()
  stub_vim{lines = {"", "# a comment", ""}, cursor = {1, 3}, ft = "ruby"}
  complete()
  it("creates the frozen_string_literal directive", function()
    assert.are.same({"# frozen_string_literal: true", "", "# a comment", ""}, api.lines(1, 6))
    assert.are.same({2, 999}, api.cursor())
  end)
end)

insulate("empty first line → # frozen_string_literal: true", function()
  stub_vim{lines = {"  ", "# a comment", ""}, cursor = {1, 3}, ft = "ruby"}
  complete()
  it("creates the frozen_string_literal directive", function()
    assert.are.same({"# frozen_string_literal: true", "", "# a comment", ""}, api.lines(1, 6))
    assert.are.same({2, 999}, api.cursor())
  end)
end)

insulate("let binding add parens and accolades", function()
  stub_vim{lines = {"", "  let :xxx"}, cursor = {2, 2}, ft = "ruby"}
  complete()
  it("adds the parens and an empty block", function()
    --                  0....+....1....+
    assert.are.same({"","  let(:xxx) {  }"}, api.lines(1, 2))
    assert.are.same({2, 13}, api.cursor())
  end)
end)

insulate("memo", function()
  stub_vim{lines = {"  memo xxx"}, cursor = {1, 3}, ft = "ruby"}
  complete()
  it("creates a stub", function()
    assert.are.same({"  def xxx", "    @__xxx__ ||=", "      ", "  end"}, api.lines(1, 4))
    assert.are.same({3, 999}, api.cursor())
  end)
end)
