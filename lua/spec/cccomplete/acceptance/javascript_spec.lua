-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local complete = require'cccomplete'.complete
local api = require'nvimapi'
-- local r = require'spec.helpers.random'


insulate("describe completion", function()
  stub_vim{lines = {"mocha"}, cursor = {1, 999}, ft = "javascript"}
  complete()
  it("has added and modified the lines", function()
    assert.are.same({
      'import mocha_ from "mocha"',
      'const {mocha} = mocha_',
      'import chai_ from "chai"',
      'const expect = chai_.expect',
      ''
      }, api.lines(1, 5))
    assert.are.same({5, 999}, api.cursor())
  end)
end)


