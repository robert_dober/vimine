-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local complete = require'cccomplete'.complete
local api = require'nvimapi'
local r = require'spec.helpers.random'

insulate("module completion", function()
  stub_vim{lines = {"module", ""}, cursor = {1, 3}, ft = "haskell", path = "test/Module/ATestSpec.hs"}
  complete()
  it("has added and modified the lines", function()
    assert.are.same({
      "module Module.ATestSpec",
      "    (spec",
      "    ) where",
      "",
      "import Test.Hspec",
      "",
      "spec :: Spec",
      "spec = do",
"    describe "}, api.lines(1, 9))
    assert.are.same({9, 999}, api.cursor())
  end)
end)

insulate("describe completion", function()
  stub_vim{lines = {"", '    describe "some"'}, cursor = {2, 3}, ft = "haskell"}
  complete()
  it("has added and modified the lines", function()
    assert.are.same({
      "",
      '    describe "some" $ do',
      "        ",
      }, api.lines(1, 3))
    assert.are.same({3, 999}, api.cursor())
  end)
end)

insulate("abbrev completion", function()
  stub_vim{lines = {"", " expression =="}, cursor = {2, 3}, ft = "haskell", path = "test/Module/ATestSpec.hs"}
  complete()
  it("has added and modified the lines", function()
    assert.are.same({
      "",
      " expression `shouldBe` ",
      }, api.lines(1, 2))
    assert.are.same({2, 999}, api.cursor())
  end)
end)

insulate("default completion", function()
  stub_vim{lines = {""}, cursor = {1, 3}, ft = "haskell", path = "test/Module/ATestSpec.hs"}
  complete()
  it("has added and modified the lines", function()
    assert.are.same({
      " do", "  ",
      }, api.lines(1, 2))
    assert.are.same({2, 999}, api.cursor())
  end)
end)

-- SPDX-License-Identifier: Apache-2.0
