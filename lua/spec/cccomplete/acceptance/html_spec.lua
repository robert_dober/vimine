-- local dbg = require("debugger") 
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local complete = require'cccomplete'.complete
local api = require'nvimapi'

insulate("first line makes template", function()
  it("creates the template, if first line is empty", function()
  stub_vim{lines = {'  ', ""}, cursor = {1, 1}, ft = "html"}
  complete()
    local expected = {
      '<!DOCTYPE html>',
      '<html>',
      '  <head>',
      '    <title>INSERT TITLE HERE</title>',
      '    <meta charset="UTF-8">',
      '    <meta name="viewport" content="width=device-width, initial-scale=1">',
      '    <!-- <link rel="stylesheet" href="INSERT URI HERE"> -->',
      '  </head>',
      '  <body>',
      '    ', -- row #10
      '  </body>',
      '</html>'
    }
    assert.are.same(expected, api.lines(1, 12))
    assert.are.same({10, 999}, api.cursor())
  end)
  it("does not creates the template, if first line is not empty", function()
    stub_vim{lines = {' html ', ""}, cursor = {1, 2}, ft = "html"}
    complete()
    local expected = { ' html ', '' }
    assert.are.same(expected, api.lines(1, 2))
    assert.are.same({1, 2}, api.cursor())
  end)
  it("does not create the template if not in first line", function()
    lines = {"", ""}
    stub_vim{lines = lines, cursor = {2, 2}, ft = "html"}
    complete()
    local expected = lines
    assert.are.same(expected, api.lines(1, 2))
    assert.are.same({2, 2}, api.cursor())
  end)
end)

insulate("open close a tag", function()
  stub_vim{lines = {'  <div class="hello"', ""}, cursor = {1, 6}, ft = "html"}
  complete()
  it("creates a block", function()
    assert.are.same({'  <div class="hello">', '    ', '  </div>', ''}, api.lines(1, 4))
    assert.are.same({2, 999}, api.cursor())
  end)
end)
insulate("close a tag", function()
  stub_vim{lines = {'<span>  ', ""}, cursor = {1, 6}, ft = "html"}
  complete()
  it("creates a block", function()
    assert.are.same({'<span>', '  ', '</span>', ''}, api.lines(1, 4))
    assert.are.same({2, 999}, api.cursor())
  end)
end)
