-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub = require'spec.helpers.stub_vim'
local stub_vim = stub.stub_vim

local complete = require'cccomplete'.complete
local api = require'nvimapi'

describe("parens", function()
  stub_vim{lines = {"  fcall("}, cursor = {1, 999}, ft = "elixir"}
  complete()
  it("expands to an aligned closing paren", function()
    assert.are.same({"  fcall(", "    ", "  )"}, api.buffer())
    assert.are.same({2, 999}, api.cursor())
  end)
end)

describe("parens with ws", function()
  stub_vim{lines = {"  fcall( "}, cursor = {1, 999}, ft = "elixir"}
  complete()
  it("expands to an aligned closing paren", function()
    assert.are.same({"  fcall(", "    ", "  )"}, api.buffer())
    assert.are.same({2, 999}, api.cursor())
  end)
end)

describe("parens with bracket", function()
  stub_vim{lines = {"    sequence(["}, cursor = {1, 999}, ft = "elixir"}
  complete()
  it("expands to an aligned closing paren", function()
    assert.are.same({"    sequence([", "      ", "    ])"}, api.buffer())
    assert.are.same({2, 999}, api.cursor())
  end)
end)

describe("parens with bracket and ws", function()
  stub_vim{lines = {"  fcall([ "}, cursor = {1, 999}, ft = "elixir"}
  complete()
  it("expands to an aligned closing paren", function()
    assert.are.same({"  fcall([", "    ", "  ])"}, api.buffer())
    assert.are.same({2, 999}, api.cursor())
  end)
end)

describe("parens with accolade and ws", function()
  stub_vim{lines = {"  fcall({ "}, cursor = {1, 999}, ft = "elixir"}
  complete()
  it("expands to an aligned closing paren", function()
    assert.are.same({"  fcall({", "    ", "  })"}, api.buffer())
    assert.are.same({2, 999}, api.cursor())
  end)
end)

describe("accolades with ws", function()
  stub_vim{lines = {"  fcall{ "}, cursor = {1, 999}, ft = "elixir"}
  complete()
  it("expands to an aligned closing paren", function()
    assert.are.same({"  fcall{", "    ", "  }"}, api.buffer())
    assert.are.same({2, 999}, api.cursor())
  end)
end)

describe("brackets", function()
  stub_vim{lines = {"  fcall["}, cursor = {1, 999}, ft = "elixir"}
  complete()
  it("expands to an aligned closing paren", function()
    assert.are.same({"  fcall[", "    ", "  ]"}, api.buffer())
    assert.are.same({2, 999}, api.cursor())
  end)
end)
-- SPDX-License-Identifier: Apache-2.0
