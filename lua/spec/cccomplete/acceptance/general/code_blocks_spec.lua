-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub = require'spec.helpers.stub_vim'
local stub_vim = stub.stub_vim
local stubber = stub.stubber

local complete = require'cccomplete'.complete
local api = require'nvimapi'

describe("%codeblock", function()
  stub_vim{lines = {"  hello >", "%code sh"}, cursor = {2, 999}, ft = "???"}

  complete()
  it("expands the %code sh", function()
    assert.are.same({"  hello >", "```sh", "", "```"}, api.buffer())
    assert.are.same({3, 999}, api.cursor())
  end)
end)

-- SPDX-License-Identifier: Apache-2.0
