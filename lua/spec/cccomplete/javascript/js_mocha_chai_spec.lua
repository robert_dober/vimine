-- local dbg = require("debugger")
-- dbg.auto_where = 2
local js = require('cccomplete.javascript')()
describe("javascript", function()

  local fn_suffix = ", () => {"
  function suffixed(str)
    return str .. fn_suffix
  end

  describe("complete mocha function", function()
    local function check_mocha_completion(name, input, expected)
      local indent = string.match(input, "^%s*")
      local completion = js.complete{line = input}

      describe(name, function()
        it("has the correct lines", function()
          assert.are.same({indent .. expected, indent .. "  ", indent .. "})"}, completion.lines)
        end)
        it("has the correct offset", function()
          assert.is_equal(1, completion.offset)
        end)
        it("has the correct col", function()
          assert.is_equal(999, completion.col)
        end)
      end)
    end

    check_mocha_completion("describe", "describe 'hello'", suffixed("describe('hello'"))
    check_mocha_completion("describe", "describe('hello'", suffixed("describe('hello'"))
    check_mocha_completion('describe', 'describe("hello"', suffixed('describe("hello"'))
    check_mocha_completion("it", "it(is Monty Python's Flying Circus", suffixed("it(is Monty Python's Flying Circus"))
    -- check_mocha_completion("it with closing )", "it(is Monty Python's Flying Circus)", suffixed("it(is Monty Python's Flying Circus"))
    -- check_mocha_completion("it without opening (", "it is Monty Python's Flying Circus)", suffixed("it(is Monty Python's Flying Circus"))
    -- check_mocha_completion("setup", "setup(", "setup(function()")
    -- check_mocha_completion("teardown", "  teardown", "teardown(function()")
    -- check_mocha_completion("no multiple function", "   describe(\"hello\", function()", suffixed('describe("hello"'))

  end)
end)


