local lua = require('cccomplete.lua')()
-- local dbg = require("debugger")
-- -- Consider enabling auto_where to make stepping through code easier to follow.
-- dbg.auto_where = 2

local function complete(line)
  return lua.complete({line = line})
end

describe("shortcuts", function()
  describe("lf", function()
    it("works at beginning of the line", function()
      completion = complete("lf...")
      assert.are.same({"local function "}, completion.lines)
    end)
    it("works later too", function()
      completion = complete("  lf...")
      assert.are.same({"  local function "}, completion.lines)
    end)
  end)

end)

-- SPDX-License-Identifier: Apache-2.0
