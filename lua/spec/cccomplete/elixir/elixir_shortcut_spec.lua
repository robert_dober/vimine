-- local dbg = require("debugger")
-- dbg.auto_where = 2
local elixir = require'cccomplete.elixir'()

local function complete(line)
  return elixir.complete({line = line}).lines
end

describe("elixir", function()

  describe("shortcuts", function()
    describe("iex...", function()
      it("has the correct lines", function()
        assert.are.same({" iex> "}, complete(" iex... "))
      end)
      it("has the correct lines, space not needed", function()
        assert.are.same({"  iex>"}, complete("  iex..."))
      end)
    end)
    describe("iex does not work", function()
      it("default do completion", function()
        assert.are.same({"  iex do", "    ", "  end"}, complete("  iex "))
      end)
    end)
    describe("@...", function()
      it("has the correct lines", function()
        assert.are.same({" |>"}, complete(" @..."))
      end)
      it("no good if it does not work in the middle", function()
        assert.are.same({"some text |>"}, complete("some text @..."))
      end)
    end)
  end)
end)

-- SPDX-License-Identifier: Apache-2.0
