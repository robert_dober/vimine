-- local dbg = require("debugger")
-- dbg.auto_where = 2
local elixir = require'cccomplete.elixir'()

local subject

local function complete(line)
  return elixir.complete({line = line})
end

local function it_behaves_like_doctest(opts)
  local offset = opts.offset or 1
  local col    = opts.col or 999
  it("has correct lines", function()
    assert.are.same(opts.lines, subject.lines)
  end)
  it("has correct offset", function()
    assert.is_equal(subject.offset, offset)
  end)
  it("has correct col", function()
    assert.is_equal(subject.col, col)
  end)
end

describe("elixir", function()
  describe("unnumbered first line too little indent", function()
    local line = "```elixir"
    subject = complete(line)
    it_behaves_like_doctest{lines = {"```elixir", "", "```"}}
  end)
end)
--SPDX-License-Identifier: Apache-2.0
