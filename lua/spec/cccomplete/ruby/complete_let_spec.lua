-- local dbg = require("debugger")
-- dbg.auto_where = 2

local subject = require'cccomplete.ruby.complete_let'

-- describe("no let → input is output", function()
--   it("for empty lines", function()
--    local cursor = {math.random(1, 100), math.random(1, 100)}
--    local line = ""
--    assert.are.same(subject{line = line, cursor = cursor}, {lines = {line}, offset = 0, col = cursor[2]})
--   end)
--   it("for other lines", function()
--     local cursor = {math.random(1, 100), math.random(1, 100)}
--     local line = "  # let xxx"
--     assert.are.same(subject{line = line, cursor = cursor}, {lines = {line}, offset = 0, col = cursor[2]})
--   end)
-- end)

describe("let at the beginning", function()
  it("for symbolic", function()
    local cursor = {1, 999}
    local line = "let :xxx"
    --                         0....+....1..
    local expected = {lines = {"let(:xxx) {  }"}, cursor = {1, 12}}
    local result = subject{line = line, cursor = cursor}
    assert.are.same(expected.lines, result.lines)
    assert.is_equal(0, result.offset)
    assert.is_equal(11, result.col)
  end)
  it("for not symbolic", function()
    local cursor = {1, 999}
    local line = "let xxx_yyy"
    --                         0....+....1....+.
    local expected = {lines = {"let(:xxx_yyy) {  }"}, cursor = {1, 12}}
    local result = subject{line = line, cursor = cursor}
    assert.are.same(expected.lines, result.lines)
    assert.is_equal(0, result.offset)
    assert.is_equal(15, result.col)
  end)
end)
