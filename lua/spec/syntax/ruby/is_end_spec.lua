-- local dbg = require("debugger")
-- dbg.auto_where = 2

local is_end = require'syntax.ruby'.is_end

local function is_truthy(input)
  local result = not not is_end(input)
  assert.is_true(result)
end

local function is_falsy(input)
  local result = not not is_end(input)
  assert.is_false(result)
end

describe("is_end", function()
  it("needs to be a line with end only", function()
    is_truthy("end")
    is_truthy("end   ")
    is_truthy(" end")
    is_truthy("  end ")
  end)
  it("must not have other non ws chars", function()
    is_falsy("endx")
    is_falsy("end   .")
    is_falsy("end x")
    is_falsy("   x end")
    is_falsy("   x end y")
  end)
  it("also needs to contain end", function()
    is_falsy("en")
    is_falsy("ed ")
    is_falsy(" nd")
    is_falsy("e")
    is_falsy("n")
    is_falsy("  d ")
    is_falsy("den")
    is_falsy("edn")
    is_falsy("edn ?")
  end)
end)

--SPDX-License-Identifier: Apache-2.0

