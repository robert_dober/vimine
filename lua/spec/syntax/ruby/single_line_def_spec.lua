-- local dbg = require("debugger")
-- dbg.auto_where = 2

local subject = require'syntax.ruby'.single_line_def

local function assert_falsy(line)
  assert.is_false(subject(line) or false)
end

local function assert_parts(line, ...)
  local lhs, rhs = subject(line)
  assert.are.same({...}, {lhs, rhs})
end

describe("is_single_line_def", function()
  it("needs to start with def", function()
    assert_falsy("k def")
    assert_falsy("ef")
  end)
  it("needs to contain an =", function()
    assert_falsy("def x(y)")
    assert_falsy("def x)")
    assert_falsy("  def a(y = 2)") -- tricky one
  end)
  it("satisfies all of the above", function()
    assert_parts("def x(y) =", "def x(y)", "")
    assert_parts(" def x(y=nil) =", " def x(y=nil)", "")
    assert_parts("def x(y=nil) = 42", "def x(y=nil)", "42")
    assert_parts("def x(y=nil) = x == 1 ? 'yes' : 'no'", "def x(y=nil)", "x == 1 ? 'yes' : 'no'")
  end)

end)
--SPDX-License-Identifier: Apache-2.0
