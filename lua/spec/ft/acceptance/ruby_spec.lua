-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local ruby = require'ft.ruby'
local api = require'nvimapi'

insulate("module_structure", function()
  stub_vim{lines = {"", "# a comment", ""}, cursor = {2, 3}, ft = "ruby", path="lib/a_b/c_de.rb"}
  ruby.make_module_structure()
  it("has inserted the lines", function()
    assert.are.same({"", "module AB", "  module CDe", "    ", "  end", "end", ""}, api.lines(1, 7))
    assert.are.same({4, 999}, api.cursor())
  end)
end)

insulate("toggle method type", function()
  context("from one line to block", function()
    stub_vim{lines = {"", "  def some_method(*) = x"}, cursor = {2, 3}, ft = "ruby"}
    ruby.toggle_method_type()
    it("has changed the method into a block method", function()
      assert.are.same({"", "  def some_method(*)", "    x", "  end"}, api.lines(1, 4))
      assert.are.same({3, 999}, api.cursor())
    end)
  end)
  context("from one block to line", function()
    stub_vim{lines = {"", "  def some_method(*)", "    x", "  end"}, cursor = {2, 3}, ft = "ruby"}
    ruby.toggle_method_type()
    it("has changed the method into a block method", function()
      assert.are.same({"", "  def some_method(*) = x"}, api.lines(1, 2))
      assert.are.same({2, 999}, api.cursor())
    end)
  end)
  context("does not change multi line block methods", function()
    lines = {"", "  def some_method(*)", "    x", " y", " end"}
    stub_vim{lines = lines, cursor = {2, 3}, ft = "ruby"}
    ruby.toggle_method_type()
    it("does nothing", function()
      assert.are.same(lines, api.lines(1, 5))
      assert.are.same({2, 3}, api.cursor())
    end)
  end)

end)
-- SPDX-License-Identifier: Apache-2.0
