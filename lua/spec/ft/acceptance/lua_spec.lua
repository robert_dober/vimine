-- local dbg = require("debugger")
-- dbg.auto_where = 2

local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local api = require'nvimapi'
local new_file = require'ft.lua.new_file'

describe('new_file', function()
  before_each(function()
    stub_vim{lines = {""}, cursor = {1, 3}}
    new_file()
  end)
  it('creates two lines', function()
    expected_lines = {
     '-- local dbg = require("debugger")',
     '-- dbg.auto_where = 2',
     ''
   }
    assert.are.same(expected_lines, api.lines(1, 4))
  end)
  it('sets the cursor', function()
    assert.are.same({3, 999}, api.cursor())
  end)
end)

