-- local dbg = require("debugger")
-- dbg.auto_where = 2
local stub_vim = require'spec.helpers.stub_vim'.stub_vim
local yaml = require'ft.yaml'
local api = require'nvimapi'

describe("disable_cop_in_rubocop", function()
  stub_vim{lines = {"whatever", "  this is the cop"}, cursor = {2, 9}, ft = "yaml"}
  yaml.disable_cop_in_rubocop()
  it("aligns and inserts", function()
    assert.are.same({"whatever", "this is the cop:", "  Enabled: false", ""}, api.lines(1, 4))
    assert.are.same({4, 999}, api.cursor())
  end)
end)
describe("no double colons at the end", function()
  stub_vim{lines = {"whatever", "  this is the cop:"}, cursor = {2, 9}, ft = "yaml"}
  yaml.disable_cop_in_rubocop()
  it("aligns and inserts", function()
    assert.are.same({"whatever", "this is the cop:", "  Enabled: false", ""}, api.lines(1, 4))
    assert.are.same({4, 999}, api.cursor())
  end)
end)
describe("does not remove lines after the insert", function()
  
  stub_vim{lines = {"whatever", "  this is the cop:", "afterwards"}, cursor = {2, 9}, ft = "yaml"}
  yaml.disable_cop_in_rubocop()
  it("aligns and inserts", function()
    assert.are.same({"whatever", "this is the cop:", "  Enabled: false", "", "afterwards"}, api.lines(1, 5))
    assert.are.same({4, 999}, api.cursor())
  end)
end)
--SPDX-License-Identifier: Apache-2.0
