-- local dbg = require("debugger")
-- dbg.auto_where = 2

local function writefile(name, strings)
  local file = assert(io.open(name, "w"))
  for _, str in ipairs(strings) do
    file:write(str)
  end
  file:close()
end

return {
  writefile = writefile
}
