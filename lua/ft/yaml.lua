-- local dbg = require("debugger")
-- dbg.auto_where = 2

local disable_cop = require 'ft.yaml.rubocop'.disable_cop
local context = require'context'.context

local function disable_cop_in_rubocop()
  local ctxt = context()
  local result = disable_cop(ctxt.line)
  ctxt.api.set_lines(ctxt.lnb, ctxt.lnb, result.lines)
  ctxt.api.set_cursor(ctxt.lnb+result.offset-1, 999)
end

return {
  disable_cop_in_rubocop = disable_cop_in_rubocop
}
--SPDX-License-Identifier: Apache-2.0
