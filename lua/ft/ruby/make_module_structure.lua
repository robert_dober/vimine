-- local dbg = require("debugger")
-- dbg.auto_where = 2

local camelize = require'tools.active_support'.camelize
local context = require'context'.context
local F = require'tools.functors'
local map = require'tools.fn'.map
local split = require'tools.string'.split
local L = require'tools.list'
local indent = require'cccomplete.helpers'().indent

local function make_modules(names)
  local prefix = {}
  local suffix = {}
  local infix = nil
  for i, name in ipairs(names) do
    local indent = string.rep("  ", i-1)
    table.insert(prefix, indent .. "module " .. name)
    table.insert(suffix, indent .. "end")
    infix = indent .. "  "
  end
  return {
    lines = L.concat(prefix, {infix}, L.reverse(suffix)),
    offset = #names
  }
end

local function make_lines_from_name(path)
  local segments = split(string.gsub(path, "%.rb$", ""), "/")
  local relevant = L.tail_from(segments, F.element_fn{"lib", "spec"})
  local names = map(relevant, camelize)
  return make_modules(names)
end

local function make_module_structure()
  local ctxt  = context()
  local api   = ctxt.api
  local result = make_lines_from_name(ctxt.file_path)
  api.set_lines(ctxt.lnb, ctxt.lnb, result.lines)
  api.set_cursor(ctxt.lnb+result.offset, 999)
end

return make_module_structure
--SPDX-License-Identifier: Apache-2.0

