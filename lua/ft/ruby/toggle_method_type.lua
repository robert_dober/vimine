-- local dbg = require("debugger")
-- dbg.auto_where = 2

local context = require'context'.context
local syntax = require'syntax.ruby'
local indent = require'cccomplete.helpers'().indent

local function convert_to_block(head, definition, lnb)
  local lines = {
    head,
    indent(head) .. "  " .. definition, 
    indent(head) .. "end"
  }
  return {
    endlnb = lnb,
    offset = 1,
    lines = lines
  }
end

local function make_def_lines(ctxt)
  local head, definition = syntax.single_line_def(ctxt.line)
  if head then
    return convert_to_block(head, definition, ctxt.lnb)
  end
end

local function make_single_line_def(ctxt)
  local next_line = ctxt.next_line()
  if syntax.is_end(next_line) then
    return {
      endlnb = ctxt.lnb + 1,
      offset = 0,
      lines = {ctxt.line .. " ="}
    }
  end
  if syntax.is_end(ctxt.next_line(2)) then
    local stripped = string.gsub(ctxt.next_line(), "^%s*", "")
    return {
      endlnb = ctxt.lnb + 2,
      offset = 0,
      lines = {ctxt.line .. " = " .. stripped}
    }
  end
end

local function toggle_method_type()
  local ctxt  = context()
  local api   = ctxt.api
  if not syntax.is_def(ctxt.line) then
    return
  end

  local result = make_def_lines(ctxt)
  result = result or make_single_line_def(ctxt)

  if result then
    api.set_lines(ctxt.lnb, result.endlnb, result.lines)
    api.set_cursor(ctxt.lnb+result.offset, 999)
  end
end

return toggle_method_type
--SPDX-License-Identifier: Apache-2.0

