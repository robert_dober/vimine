-- local dbg = require("debugger")
-- dbg.auto_where = 2

local api = require'nvimapi'
return function()
  local lines = {
    '-- local dbg = require("debugger")',
    '-- dbg.auto_where = 2',
    '',
  }
  api.set_lines(1, 2, lines)
  api.set_cursor(3)
end

