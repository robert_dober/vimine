-- local dbg = require("debugger")
-- dbg.auto_where = 2

local strip = require'tools.string'.strip

local function disable_cop(line)
  local new_line = string.gsub(strip(line) .. ":", ":+$", ":")
  return {
    lines = {
      new_line,
      "  Enabled: false",
      ""
    },
    offset = 3
  }
end

return {
  disable_cop = disable_cop
}

--SPDX-License-Identifier: Apache-2.0
