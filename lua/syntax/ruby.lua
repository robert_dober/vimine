-- local dbg = require("debugger")
-- dbg.auto_where = 2

local curry = require'tools.fn'.curry

local function is_prefix_keyword(keyword, line)
  if not line then return end
  return string.match(line, "^%s*" .. keyword .. "%s")
end

local function is_single_keyword(keyword, line)
  if not line then return end
  return string.match(line, "^%s*" .. keyword .. "%s*$")
end

local function equal_sign_on_appropriate_place(line)
  return string.match(line, "%b().*%s=") or
  string.match(line, "def%s[^(]*%s=")
end

local function single_line_def(line)
  local with_parens, rest = string.match(line, "^(%s*def%s+[^=]+%b())%s+=%s*(.*)")
  if with_parens then
    return with_parens, rest
  end
  local wo_parens, rest = string.match(line, "^(%s*def%s+[^(=]+)%s+=%s*(.*)")
  if rest then
    return wo_parens, rest
  end
end

return {
  is_end = curry(is_single_keyword, "end"),
  is_def = curry(is_prefix_keyword, "def"),
  single_line_def = single_line_def
}
