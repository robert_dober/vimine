-- local dbg = require("debugger")
-- dbg.auto_where = 2

local fn = require"tools.fn"
local push = require"tools.list".push

function add(element)
end
function carthesian_pair(l, r)
  local result = {}
  for _, lval in ipairs(l) do
    for _, rval in ipairs(r) do
      table.insert(result, {lval, rval})
    end
  end
  return result
end

function extend_product_with(product, t)
  local result = {}
  for _, pval in ipairs(product) do
    for _, val in ipairs(t) do
      local row = push(pval, val)
      table.insert(result, row)
    end
  end
  return result
end

function carthesian_product(ls)
  local head = table.remove(ls, 1)
  local tail = table.remove(ls, 1)
  return fn.reduce(ls, carthesian_pair(head, tail), extend_product_with)
end

return {
  carthesian_product = carthesian_product,
}

