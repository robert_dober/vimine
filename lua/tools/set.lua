local function set (list)
  local set = {}
  for _, l in ipairs(list) do set[l] = true end
  return set
end

local function member(set, element)
  if set[element] then
    return true
  else
    return false
  end
end
return {
  member = member,
  set = set
}
