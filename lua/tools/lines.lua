-- local dbg = require("debugger")
-- dbg.auto_where = 2

local Pair = require'objects.pair'
local map = require'tools.fn'.map
local flatten = require'tools.list'.flatten
local sm = require'tools.state_machine'.state_machine

local function prefix_remover_and_nl_adder(n)
  return function(subject)
    return string.sub(subject, n+1) .. "\n"
  end
end

local function align_to_first_line(lines)
  local prefix = string.gsub(lines[1], "%S.*", "")
  return map(lines, prefix_remover_and_nl_adder(string.len(prefix)))
end

local function lines_into_sections(header, lines)
  local function add_to_current_section(line, match, acc)
    local head_pair = acc[1]
    local lines = head_pair.right
    table.insert(lines, line)
    return acc
  end
  local function new_section(line, match, acc)
    local new_pair = Pair:new{left=match, right={line}}
    table.insert(acc, 1, new_pair)
    return acc
  end

  local state_machine = sm{
    start = {
      {header, new_section},
      {".*", add_to_current_section}
    }
  }
  return state_machine(lines, {Pair:new{left="", right={}}})
end

local function sorted_sections(header, lines)
  local groups = lines_into_sections(header, lines)
  table.sort(groups, Pair.smaller)
  local sorted = map(groups, function(pair) return pair.right end)
  return flatten(sorted)
end

return {
  align_to_first_line = align_to_first_line,
  lines_into_sections = lines_into_sections,
  sorted_sections = sorted_sections
}
