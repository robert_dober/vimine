-- local dbg = require("debugger")
-- dbg.auto_where = 2

local Set = require'tools.set'

local function const_fn(retval)
  return function()
    return retval
  end
end
local function element_fn(list)
  local set = Set.set(list)
  return function(element)
    return Set.member(set, element)
  end
end
local function equals(subject)
  return function(with)
    return subject == with
  end
end
local function identity(subject)
  return function()
    return subject
  end
end
local function negate(afun)
  return function(subject)
    return not(afun(subject))
  end
end

return {
  const_fn = const_fn,
  element_fn = element_fn,
  equals = equals,
  identity = identity,
  negate = negate,
}
