-- local dbg = require("debugger")
-- dbg.auto_where = 2
--
local split = require 'tools.string'.split
local map   = require 'tools.fn'.map
local tail_from = require'tools.list'.tail_from
local F = require'tools.functors'

local function capitalize(str)
    return string.upper(string.sub(str, 1, 1)) .. string.sub(str, 2)
end

local function camelize(name)
  local parts = split(name, "_")
  return table.concat(map(parts, capitalize))
end

local function camelize_path(path, joiner, delimiter)
  local joiner = joiner or "."
  local delimiter = delimiter or {}
  local path = string.gsub(path, "[.][^.]*$", "")
  local segments = split(path, "/")
  local relevant = tail_from(segments, F.element_fn(delimiter))
  return table.concat(map(relevant, camelize), joiner)
end

return {
  camelize      = camelize,
  camelize_path = camelize_path,
}
