-- local dbg = require("debugger")
-- dbg.auto_where = 2
local foldl = require'tools.fn'.foldl
local S = require'tools.string'

local function parse_one_arg(into, arg)
  if string.match(arg, "^:") then
    into[1][string.gsub(arg, "^:", "", 1)]=true
    return into
  end
  if string.match(arg, "^%w+:") then
    local key = string.gsub(arg, ":.*", "")
    local value = string.gsub(arg, "^%w+:", "")
    into[1][key]=value
    return into
  end
  table.insert(into[2], arg)
  return into
end
local function parse_args(arg_string)
  local args = S.split(arg_string)
  return foldl(args, parse_one_arg, {{}, {}})
end



return {
  parse_args = parse_args,
}
