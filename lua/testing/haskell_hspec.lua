-- local dbg = require("debugger")
-- dbg.auto_where = 2

local context = require'context'
local api = require'nvimapi'
return {
  run = function(variable)
    local ctxt = context.context()
    -- print("context.line " .. context.line)
    local test_command =  "stack build && stack runghc Spec.hs"
    local pattern = string.match(ctxt.line, '^%s*it%s"(.*)".*')
    -- print(pattern)
    if pattern then
      test_command = test_command .. " -- -m " .. '"' .. pattern .. '"'
    end
    api.set_var(variable, test_command)
  end
}

