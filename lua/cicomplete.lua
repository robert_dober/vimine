-- local dbg = require("debugger")
-- dbg.auto_where = 2
local context = require'context'.context
local complete = require'cicomplete.general'

local function cicomplete()
  local ctxt  = context()
  local api = ctxt.api
  local result = complete(ctxt)
  if result then
    api.set_current_line(result.line)
    api.set_cursor(result.col)
  end
end

return {
  cicomplete = cicomplete,
}
