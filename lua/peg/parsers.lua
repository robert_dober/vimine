-- local dbg = require("debugger")
-- dbg.auto_where = 2

local S = require'tools.string'

local function ht(a_string)
  return unpack(S.fst_and_rst(a_string))
end
local function char_parser(the_char_to_parse)
  return function(input, state)
    local h, t = ht(input)
    if h == the_char_to_parse then
      return {h, t}
    end
  end
end

local function pattern_parser(pattern)
  local anchored_pattern = "^(" .. pattern .. ")(.*)"
  return function(input)
    local parsed, rest = string.match(input, anchored_pattern)
    if parsed then
      return {parsed, rest}
    end
  end
end

return {
  char_parser = char_parser,
  pattern_parser = pattern_parser,
}

