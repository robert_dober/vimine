-- local dbg = require("debugger")
-- dbg.auto_where = 2

local function choice(parsers)
  return function(input)
    local result
    for _, parser in ipairs(parsers) do
      result = parser(input)
      if result then
        return result
      end
    end
  end
end

local function many(parser)
  return function(input)
    local result = ""
    local rest = input
    local x = parser(rest)
    while x do
      result = result .. x[1]
      rest = x[2]
      x = parser(rest)
    end
    return {result, rest}
  end
end

local function map(parser)
  return function(input)
    local result = {}
    local rest = input
    local x = parser(rest)
    while x do
      table.insert(result, x[1])
      rest = x[2]
      x = parser(rest)
    end
    return {result, rest}
  end
end

local function apply(parser, mapper)
  return function(input)
    local result = parser(input)
    if result then
      return {mapper(result[1]), result[2]}
    end
  end
end

local function sequence(parsers)
  return function(input)
    local result = ""
    local rest = input
    for _, parser in ipairs(parsers) do
      local result1 = parser(rest)
      if result1 then
        result = result .. result1[1]
        rest = result1[2]
      else
        return nil
      end
    end
    return {result, rest}
  end
end

local function skip(parser)
  return function(input)
    local result = parser(input)
    if result then
      return {"", result[2]}
    end
  end
end

local function some(parser, min_count)
  local min_count = min_count or 1
  return function(input)
    local result = many(parser)(input)
    if result then
      local parsed, rest = unpack(result)
      if string.len(parsed) >= min_count then
        return {parsed, rest}
      end
    end
  end
end

local function tagged(parser, tag)
  return function(input)
    local result = parser(input)
    if result then
      return {{tag, result[1]}, result[2]}
    end
  end
end

return {
  apply = apply,
  choice = choice,
  many = many,
  map = map,
  sequence = sequence,
  skip =skip,
  some = some,
  tagged = tagged
}
-- SPDX-License-Identifier: Apache-2.0
