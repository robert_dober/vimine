-- local dbg = require("debugger")
-- dbg.auto_where = 2

local Pair = {left = nil, right = nil}


function Pair:new(o)
  o = o or Pair
  setmetatable(o, self)
  self.__index = self
  return o
end

function Pair:set_left(left)
  return Pair.new{left = left, right = self.right}
end

function Pair:set_right(right)
  return Pair.new{left = self.left, right = right}
end

function Pair.smaller(l, r)
  -- dbg()
  if (l.left < r.left) then return true end
  return false
end

function Pair:set(prop, f)
  self[prop] = f(self[prop])
end


return Pair
--SPDX-License-Identifier: Apache-2.0
