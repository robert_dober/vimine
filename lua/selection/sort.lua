-- local dbg = require("debugger")
-- dbg.auto_where = 2

local api = require'nvimapi'
local buffer = require'tools.lines'
local sorted_sections = require'tools.lines'.sorted_sections

return {
  sort_by_header = function(header, first, last)
    local lines = api.lines(first, last)
    local result = sorted_sections(header, lines)
    api.set_lines(first, last, result)
  end
}
--SPDX-License-Identifier: Apache-2.0
